

In this chapter, our presentation of Markov random fields loosely follows [\citenum{glocker2011random}] which provides a good overview of this topic generally and its applications to image registration problems in particular. As mentioned in the previous chapter, the segmentation component in our framework could be enhanced through carefully modelling local dependencies. In particular, we would like a model that naturally penalizes adjacent pixels that do not share the same segmentation label; Markov random fields (MRFs) are such models. In addition to modelling the segmentation component of our framework, they can also be used to model the registration component and by imposing some hard constraints on the displacement field during training, we can guarantee the retrieved deformation to be diffeomorphic [\citenum{choi2000injectivity}]. Furthermore, the energy functional of these MRFs can be linearized [\citenum{glocker2008dense}] turning our simultaneous estimation framework into an iterative efficient linear programming optimization problem that recovers the lowest potential of the energy functional. In this chapter, we introduce the general theory of MRFs and how they can be used to model image segmentation and image registration problems through efficient linear programming.



\section{Random Fields} 
In its most general form, a random field is a set of random variables indexed by elements of a reference space. A random field is a generalization of a unidimensional indexed stochastic process. A stochastic process is a set of random variables indexed by $\R$ , typically interpreted as \textit{time}. Rather than thinking of the stochastic behaviour of random variables over time (a process), we can more generally think of the stochastic behaviour of random variables over a space (a field) or index set. Random fields are often considered in graphical models, where the set of random variables is indexed by the vertex set of some specified simply connected (no loops or multiple edges) undirected graph and the edge set of the graph represents some relationship among the vertices. In Section 2 we will come to see that the edge set, or more specifically the neighbourhood structure of this graph, represents some special relationship between the vertices which translates into a regularization we would like to model into our vision problem. Although the general definition of a random field doesn't impose this graph-theoretic approach, the literature often does; this is due to their wide applications in graphical models. That is, when a random field is said to be a set of random variables, these random variables are thought to be indexed by the vertex set of some undirected graph. For the remainder of this chapter, we will think of random fields in this way. \\

Consider a random field $\mathbf{X}$, which denotes a finite set of random variables and is indexed by a set of vertices $V$ of an undirected graph, further let $X_i \in \mathbf{X}$ be a random variable with support $L_i$. We can then define a probability distribution over $L_i$, denoted $\mathbb{P}(X_i=x_i)$. More generally we can define a joint probability distribution over the Cartesian product of the  supports. Let $\mathcal {X}  = \times_{i \in V} L_i $, the Cartesian  product of all supports, the joint probability is then denoted by $\mathbb{P}(\mathbf{X} = \mathbf{x} )$ for some $\mathbf{x} \in \mathcal{X}$. $\mathcal{X}$ is also known as a labelling or configuration of the random field and $\mathcal {X}$ is the label space or configuration space. In our case, we will be using random fields to model some real-world vision problem, in which the solution will be reduced to a labelling optimization problem, thus searching through $\mathcal{X}$ for some optimal labelling $\mathbf{x}$.


\section{Neighbourhood Structures}

Let $G=(V,E)$ be the graph corresponding to the random field in the previous section, that is $V$ is the vertex set such that there is a one-to-one correspondence between the vertices and the random variables in $\mathbf{X}$ and $E$ is the edge set. It is natural to consider the neighbourhood structure of the corresponding graph since the interconnectivity of the graph is meant to model some underlying relationships between the random variables. We define two types of subsets of $V$ in order to specify this connectivity, namely  neighbourhood sets and cliques. Two random variables $X_i,X_j \in \mathbf{X}$ are said to be neighbours if and only if  $(i,j) \in E$ (i.e. their corresponding vertices in $V$ are adjacent). For some random variable $X_i \in \mathbf{X}$, let $N(i) \subset V$ denote the set of all neighbours of $X_i$. A clique is a subset $C \subset V$ such that every pair of distinct vertices in $C$ are adjacent to each, that is, cliques are either single nodes or sets of pair-wise adjacent nodes (i.e. inducing complete sub-graphs of $G$). Let $\mathcal{C}$ denote the collection of all cliques of $G$. Furthermore, the \textit{order} of a random field  is the cardinality of the maximal clique minus one. For instance, a random field that corresponds to a regular grid graph (every interior node has exactly 4 neighbours) has an order of 1 since the maximal clique is a pair of adjacent nodes. In practice, the order of a random field significantly impacts the computational cost and for this reason, first-order random fields are the most common. For the remainder of this chapter, we only consider first-order random fields, i.e. $|C|=2$.
\begin{figure}[H]
  \centering
     \subcaptionbox{(a) First-order}{\label{fig:square-warped-a}\includegraphics[width=.28\textwidth ]  {../figures/extra_figs/order1red}} \hfill
      \subcaptionbox{(b) Second-order}{\label{fig:square-warped-a}\includegraphics[width=.28\textwidth ] {../figures/extra_figs/order2red}}\hfill
      \subcaptionbox{(b) Third-order}{\label{fig:square-warped-a}\includegraphics[width=.28\textwidth ] {../figures/extra_figs/order3red}}     
 \caption{Random fields with different orders.}
 \label{fig:toy}
\end{figure}
\section{Markov Random Fields}
We consider a particular class of random fields, Markov random fields.
\begin{definition}[Markov Random Field]\label{mattstemperflaring} 
A random field $\mathbf{X}$ is said to be a Markov random field, if and only if it satisfies the following 2 properties
\begin{align}\label{E1-mrf}
\mathbb{P}(X_i = l \;|  \mathbf{X}- \{ X_i \} ) = \mathbb{P}(X_i = l |  N(i) )             \;\;\;\;\;\;\;\;\;\;\; (Markovian\; property)                   \\
\;\mathbb{P}(\mathbf{X} = \mathbf{x}) > 0 \; \; \forall \;\mathbf{x} \in \mathcal {X}   \;      \;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\; (Positivity\; property)                  \;.
\end{align}
\end{definition}
The first property states that the conditional probability of any given variable in the field is equivalent to the conditional probability of that variable given its neighbours. Meaning that the variables depend only on their neighbours, this equivalently implies that any two nodes that are not neighbours are conditionally independent. The second property ensures that the distribution can be specified by the potentials in a Gibbs distribution. \\

Specifying the distribution of a Markov random field (MRF) in terms of the local conditional probabilities is not an easy task. Because of this in practice when MRFs are used for modelling, their probabilities are specified using a theoretical result known as the Hammersley-Clifford-Theorem [\citenum{grimmett1973theorem}]. Before presenting this result, we first define another particular class of random fields, namely the Gibbs random fields (GRFs).
\begin{definition}[Gibbs Random Field]\label{mattstemperflaring} 
A random field $\mathbf{X}$ is said to be a Gibbs random field, if and only if its probability distribution has the following form
\begin{align}\label{E2-grf}
\mathbb{P}(\mathbf{X}=\mathbf{x} ) = \frac{1}{M} \prod_{C \in \mathcal{C}}        \exp\big\{ -\psi_C(\mathbf{x}) \big\}  
\end{align}
\end{definition}
where $M$ is a normalizing constant, $\psi( \cdot)_C$ are called potential functions and $\mathcal{C}$ is the set of cliques in the graph corresponding to $\mathbf{X}$. From Equation 4.3.3 it is clear that the probability distribution of a Gibbs random field can be uniquely specified by these potential functions. Now we are ready to state the Hammersley-Clifford-Theorem.\\
 \begin{thm} [Hammersley-Clifford-Theorem]\label{mattstemperflaring}
A random field $\mathbf{X}$ is a Markov random field if and only if it is a Gibbs random field.
\end{thm}
Rather than specifying the conditional probabilities when modelling with MRFs, we can simply specify the potential functions $\psi( \cdot)_C$ . 

\section{Inference with Markov Random Fields}
Before going over how MRFs can be used to model image registration and image segmentation problems, we will first give a general methodology for modelling inference problems with MRFs. We first define a first-order Markov random field over the set $\mathbf{X} \cup \mathbf{Y}$, which is the union of two sets of random variables where the set $\mathbf{X} $ is latent and $\mathbf{Y} $ is observed. The random field has a  corresponding graph $G = (V,E)$ and set of cliques $\mathcal{C}$. Each variable in $\mathbf{X}$ has a corresponding variable in $\mathbf{Y}$ such that for a pair of corresponding variables $X_i \in \mathbf{X}$ and $Y_j \in \mathbf{Y}$ $(i, j) \in E$ and indices of $\mathbf{X} $ form a regular grid sub-graph of $G$, this structure is illustrated in Figure 4.4 (a) for 2 sets of 9 latent and observed variables.
\begin{figure}[H]
  \centering
     \subcaptionbox{(a) Markov random field}{\label{fig:square-warped-a}\includegraphics[width=.28\textwidth ]  {../figures/extra_figs/mrf}} 
      \subcaptionbox{(b) Likelihood clique}{\label{fig:square-warped-a}\includegraphics[width=.28\textwidth ] {../figures/extra_figs/clique1}}    
       \subcaptionbox{(c) Prior clique}{\label{fig:square-warped-a}\includegraphics[width=.28\textwidth ]{../figures/extra_figs/clique2}} 
    
 \caption{ Sub-figure (a) displays a graphical representation of the a Markov random field over  $\mathbf{X} \cup \mathbf{Y}$ where $| \mathbf{X}|=| \mathbf{Y}| =9$.  The $x_i$ and $y_i$ are realizations of random variables $X_i \in \mathbf{X}$ and $X_i \in \mathbf{X}$ respectively. The observed variables are represented by shaded nodes and latent variables are represented as non-shaded nodes. Sub-figure (b) displays the first type of clique, which depends on a latent variable and an observed variable. Sub-figure (b) displays the second type of clique, which depends on two neighbouring latent variables.}
 \label{fig:toy}    \mathbb{P}(\mathbf{X}=
\end{figure}     
The joint distribution of this MRF can be written as the following Gibbs distribution
\begin{align}\label{E2-grf}
\mathbb{P}(\mathbf{X}=\mathbf{x}, \mathbf{Y} =\mathbf{y}  ) = \frac{1}{M} \prod_{C \in \mathcal{C}}        \exp\big\{ -\psi_C(\mathbf{x},\mathbf{y} ) \big\}\;.
\end{align}
Moreover, since an index of a variable in $ \mathbf{Y}$ is adjacent to exactly one index of a variable in $ \mathbf{X}$ the set of cliques can be partitioned into two sets, one including cliques with indices of variables in $\mathbf{Y}$ and one without, we will denote these partitions with $\mathcal{C}_y$ and $\mathcal{C}_x$ respectively. This partition leads to the following factorization of the joint distribution
\begin{align}\label{E2-grf}
\mathbb{P}(\mathbf{X}=\mathbf{x}, \mathbf{Y} =\mathbf{y}  ) = \frac{1}{M} \prod_{C \in \mathcal{C}_y}        \exp\big\{ -\psi_C(\mathbf{x},\mathbf{y} ) \big\}    \prod_{C \in \mathcal{C}_x}        \exp\big\{ -\psi_C(\mathbf{x},\mathbf{y} ) \big\}
\end{align}
and since cliques in $\mathcal{C}_x$ do not depend on the observed variables in $\mathbf{Y}$, we can rewrite the joint distribution as follows 
\begin{align}\label{E2-grf}
\mathbb{P}(\mathbf{X}=\mathbf{x}, \mathbf{Y} =\mathbf{y}  ) = \frac{1}{M} \prod_{C \in \mathcal{C}_y}        \exp\{ -\psi_C(\mathbf{x},\mathbf{y} ) \}    \prod_{C \in \mathcal{C}_x}        \exp\{ -\psi_C(\mathbf{x}  ) \}\;.
\end{align}
The first and second product corresponds to the likelihood and prior of the MRF respectively and the $\psi_C(\mathbf{x},\mathbf{y} )$ and $\psi_C(\mathbf{x},\mathbf{y} )$ are called the \textit{likelihood} and \textit{prior potentials}. 
\subsection{MAP estimates}
Typically, a Bayesian framework is employed when modelling with MRFs, in which a Maximum a Posteriori (MAP) estimate of the random field is inferred. The idea is to model the problem such that $\hat{\mathbf{X}} = \argmax_{ \mathbf{x}   \in \mathcal{X}} p(\mathbf{X}  = \mathbf{x}  | \mathbf{Y})$ corresponds to a reasonable solution. Intuitively we are looking for the most probable configuration or labelling of the random field given we observed a set of random variables $\mathbf{Y}$. The posterior $p(\mathbf{X} | \mathbf{Y}) $ is proportional to the product of $p(\mathbf{Y} | \mathbf{X})$ (the likelihood) and $p(\mathbf{X}) $ (the prior). Since the MAP estimate is obtained by optimizing over the configuration space $\mathcal{X}$ and the distribution $p(\mathbf{Y})$ (the evidence) is free of $\mathbf{X} $ we can equivalently restate our problem as  $\hat{\mathbf{X}} = \argmax_{\mathbf{x} \in \mathcal{X}} p(\mathbf{Y}  |\mathbf{X})p(\mathbf{Y}) $. As a consequence of the Gibbs-Markov equivalence, we can specify the distribution of an MRF by specifying the likelihood and prior potential functions its corresponding Gibbs distribution.


\subsection{MRF Likelihood Distribution}
 
The likelihood models a relationship between the latent random field and the observed data. In the case of first-order random fields, every clique in $\mathcal{C}_y$ corresponds to exactly one corresponding pair of latent and observed variables and therefore the likelihood potentials can be indexed by the vertex set as follows
\begin{align}\label{E3-likelihood} 
\mathbb{P}(\mathbf{Y}=\mathbf{y}| \mathbf{X} =\mathbf{x} ) &\propto \prod_{i \in V} \exp \big\{  -\psi_i (x_i, y_i)   \big\}\\
&= \exp \Big\{ -       \sum \limits_{i \in V}      \psi_i (x_i, y_i) \Big\}\;.
\end{align}
Since the likelihood is a decreasing function of the sum of the likelihood potentials, i.e. low potentials are more probable, the likelihood potentials are usually modelled as a similarity metric between the data and the latent variables. This is similar to Gaussian parametric regression where the likelihood function is the exponential of the negative mean squared errors (MSE) and the Maximum Likelihood Estimate (MLE) corresponds to minimizing this error. The choice of likelihood potentials follows directly from the modelling assumptions of how the MRF relates to the observed data.
 

\subsection{MRF Prior Distribution}
The prior models some prior assumptions about the random field, independent of the observed data. The prior also helps to restrict the solution space to more likely solutions and in some cases (as we will see with image registration) helps to make the optimization feasible. The prior distribution has the following form 
\begin{align}\label{E4-prior-1} 
\mathbb{P}(\mathbf{X}=\mathbf{x}) \propto \prod_{\{i,j\} \in \mathcal{C}_x }   \exp \big\{  -\psi_{ij} (x_i, x_j)   \big\}\;.
\end{align}
Because of the Markovian property of MRFs, random variables that aren’t neighbours are conditionally independent, thus we can rewrite Equation 4.4.6 in terms of neighbouring vertices as follows
\begin{align}\label{E5-prior-2} 
\mathbb{P}(\mathbf{X}=\mathbf{x}) &\propto \prod_{i \in V}\prod_{j \in N(i)} \exp \{  -\psi_{ij} (x_i, x_j)   \}\\
&= \exp \Big\{ \sum _{ i \in V  }\sum \limits_{j \in N(i)} \{  -\psi_{ij} (x_i, x_j)    \Big\}\;.
\end{align}
Specifying the potentials in terms of neighbouring random variables is not only mathematically convenient, but it is also often more intuitive to the modelling assumptions and easier to interpret. For instance, in image segmentation where the random variables might be segment labels of individual pixels, neighbouring pixels could be assumed to belong to the same image segment, thus this assumption would naturally translate into a prior potential function which would be some distance metric between neighbouring pixel labels.
 
\subsection{MRF Energy Function}
Looking at equations 4.4.4 and 4.4.6 it is clear that the same labelling that maximizes the product of the prior and the likelihood, is the same labelling that minimizes the sum of the prior potentials and likelihood potentials, this sum is called the \text{energy} of the MRF and it is equivalent to the negative log probability of the MRF up to an additive constant. This is the same energy discussed in Chapter 3, Section 3.2.4 and the distribution of an MRF is indeed a Gibbs distribution. Consequently finding a MAP estimate reduces to finding a labelling that minimizes the energy.\\
 
Let $\;\;\mathbf{X}=\{ X_i \}_{i=1}^N \;\;$  be an MRF. To simplify the notation, we can omit the observed random variable from the likelihood potentials and simply let $\psi_i(x_i)$ and $\psi_{ij}(x_i, x_j)$ be the likelihood and prior potentials respectively where $x_i$ and $x_j$ are  labels. The energy function has the following form
 \begin{align}\label{E6-energy-1} 
E(x_1,…, x_N)=\sum_{i \in V} \psi_i(x_i) + \sum_{i \in V} \sum_{j \in N(i)}  \psi_{ij} (x_i, x_j)\;.
\end{align}
Under this form $\psi_i(\cdot )$ and $ \psi_{ij}(\cdot, \cdot )$ are also known as \textit{unitary} and \textit{pair-wise} potentials, respectively.
 
\subsection{Linearization of the Energy}
Searching the configuration space for an optimal labelling is computationally intensive. One way to approximate the optimal labelling is by relaxing the optimization of the energy into a continuous linear program (LP). This is attractive for at least 2 reasons, firstly the LP relaxation can often be formulated such that a unique solution exists [\citenum{glocker2008dense}] and secondly continuous  linear programs can be solved reasonably fast [\citenum{komodakis2015playing}]. Following a linearization procedure proposed in [\citenum{glocker2008dense}], we first demonstrate how the labelling problem can be turned into an integer linear program (ILP) and then relax the ILP into a continuous LP.\\

Let $L$ be the set of possible labels for any given variable in the random field. Consider 2 binary functions $\phi_i : L \to \{0,1\} $ and $\phi_{ij} : L \times L \to \{0,1\} $ such that
\begin{align}\label{E7-energy-2} 
 \phi_i(\alpha) = 
     \begin{cases}
       1 &\quad\text{if } \;\;\; x_i=\alpha    \\
       0  &\quad\text{otherwise.}\\
     \end{cases}
\end{align}
and
 \begin{align}\label{E8-energy-3}   
\phi_{ij}(\alpha, \beta) = 
     \begin{cases}
       1 &\quad\text{if }  x_i=\alpha  \text{ and } x_j=\beta    \\
       0  &\quad\text{otherwise.}\\
     \end{cases}
\end{align}
 where $ \phi_i ( \cdot)$  and $\phi_{ij} ( \cdot, \cdot)$ are indicator functions for the labels of a single variable and a pair of variables respectively. Using these indicator functions, the MRF energy can be rewritten as
\begin{align}\label{E9-energy-4} 
E(x_1,…, x_N)=  \sum_{i \in V} \sum_{\alpha \in L}  \psi_i( \alpha) \phi_i(\alpha) + \sum_{i \in V} \sum_{j  \in N(i)} \sum_{\alpha , \beta \in L}    \psi_{ij} (\alpha,\beta) \phi_{ij} (\alpha,\beta)\;.
\end{align}
Now the energy is expressed as a linear combination of  $ \phi_i ( \cdot)$  and $\phi_{ij} ( \cdot, \cdot)$, the original optimization  problem is equivalent to minimizing this new expression subject to the following equality constraints
\begin{align}\label{E10-energy-5} 
&\sum_{\alpha \in L}    \phi_{i} (\alpha) = 1    \;\;\;\;\;\;\; \forall i  \in V \\
&\sum_{\alpha \in L}    \phi_{ij} (\alpha, \beta) = \phi_{i} (\beta)    \;\;\;\;\;\;\;  \forall \beta  \in L \;\;\;    \forall i  \in V\;\;\;    \forall j \in N(i)\\
 &\sum_{\beta \in L}    \phi_{ij} (\alpha, \beta) = \phi_{i} (\alpha)    \;\;\;\;\;\;\;  \forall \alpha  \in L \;\;\;    \forall i  \in V\;\;\;    \forall j \in N(i) \;.\\
\end{align}
Since $\phi_{i} (\cdot) $ and $\phi_{ij} (\cdot, \cdot)$ are integer-valued this is an integer linear program (ILP) and ILPs are typically NP-hard. However, this form gives us a basis on which we can relax the problem into a continuous LP. In particular, rather than keeping $\phi_{i} (\cdot) $ and $\phi_{ij} (\cdot, \cdot)$  as indicator functions, we can allow them to take any non-negative value, i.e. $\phi_{i} (\cdot) \geq 0 $ and $\phi_{ij} (\cdot, \cdot) \geq 0 $. Thus the new optimization problem becomes
\begin{align}\label{E13-energy-8} 
 &\quad\text{min} \sum_{i \in V} \sum_{\alpha \in L}  \psi_i( \alpha) \phi_i(\alpha) + \sum_{i \in V}  \sum_{j \in N(i)} \sum_{\alpha , \beta \in L}    \psi_{ij} (\alpha,\beta) \phi_{ij} (\alpha,\beta)
\end{align}
         subject to:
\begin{align}\label{E14-energy-9} 
&\sum_{\alpha \in L}    \phi_{i} (\cdot) \geq 0    \;\;\;\;\;\;\; \forall i \in V \\
 &\sum_{\alpha \in L}    \phi_{ij} (\cdot, \cdot) \geq 0    \;\;\;\; \forall i \in V \\
 &\sum_{\alpha \in L}    \phi_{i} (\alpha) = 1    \;\;\;\;\;\;\; \forall i  \in V\\
 &\sum_{\alpha \in L}    \phi_{ij} (\alpha, \beta) = \phi_{i} (\beta)    \;\;\;\;\;\;\;  \forall \beta \;\;\;    \forall i  \in V\;\;\;    \forall j \in N(i)\\
 &\sum_{\beta \in L}    \phi_{ij} (\alpha, \beta) = \phi_{i} (\alpha)   \;\;\;\;\;\;\;  \forall \alpha \;\;\;    \forall i  \in V\;\;\;    \forall j  \in N(i)\;.
\end{align}
The authors of [\citenum{komodakis2015playing}] propose a primal-dual approximation algorithm for retrieving a solution for this program that is an $f$-approximation of the real optimum, meaning all instances of the problem produces a result whose value is within a factor of $f$ of the value of the optimal solution,  where $f=2\frac{d_{max}}{d_{min}}$,  $d_{max} =  \text{max }_{\alpha \neq \beta} \phi_{ij}(\alpha,\beta)$ and $d_{min} =  \text{min }_{\alpha \neq \beta} \phi_{ij}(\alpha,\beta)$.

\section{Modelling with MRFs}

In this section, we provide examples of MRFs being applied to image segmentation and image registration.

\subsection{MRFs in Image Segmentation}

In Chapter 2 Equation 2.1.4, we presented an objective function that can be used to retrieve an MRF-based image segmentation. In this section, we give further detail on this kind of image segmentation modelling while using some new concepts proposed in this chapter. Once again, we consider a binary segmentation problem. Consider an image with $N$ pixels, let $\;\;\mathbf{Y}=\{ y_i \}_{i=1}^N \;\;$ where $y_i$ is the grayscale intensity value associated with the $i^{th}$ pixel and consider a latent MRF  $\;\mathbf{X}=\{ x_i \}_{i=1}^N \;\;$ such that
\begin{align}\label{E:EPDiff} 
x_i= 
     \begin{cases}
       1 &\quad\text{if the $i^{th}$ pixel belongs to the foreground} \\
       0 &\quad\text{if the $i^{th}$ pixel belongs to the background} \;.\\
     \end{cases} 
\end{align}\\
As previously stated, specifying an MRF in a modelling exercise reduces to specifying the unitary and pairwise potentials. In knowledge-based segmentation, we use some prior knowledge about the segment classes in order to help the construction of the unitary potentials. By taking a small sample of grayscale intensity values from both classes we can estimate some simple statistics such as the mean and the variance of each segment class. Let $\{y_i^{(1)} \}_{i=1}^n$ and $\{y_i^{(0)} \}_{i=1}^m$ be samples of pixels that belong to the foreground and the background respectively. We can then estimate the following class specific parameters
\begin{align}\label{E:EPDiff} 
&\hat{\mu}_0 =\frac{1}{m} \sum\limits_{i=1}^m  y_i^{(0)}\\
&\hat{\mu}_1 =\frac{1}{n} \sum\limits_{i=1}^n  y_i^{(1)}\\
&\hat{\sigma}^2_0 =\frac{1}{m} \sum\limits_{i=1}^m  (y_i^{(0)} -\hat{\mu}_0)^2\\
&\hat{\sigma}^2_1  =\frac{1}{n} \sum\limits_{i=1}^n  (y_i^{(1)} -\hat{\mu}_1)^2
\end{align}
Now that we have some statistical information about each one of our classes, we can define the unitary potential of a pixel as a statistical distance between some attribute of the pixel and some arbitrary class. A common choice is the squared standard distance of a pixel's grayscale intensity value and a particular class. Formally the unitary potential would have the following form
\begin{align}\label{E:EPDiff} 
\psi_i(\alpha)=\left( \frac{y_i-\hat{\mu}_\alpha}{ \hat{\sigma}_\alpha}   \right)^2
\end{align}
where $\alpha \in \{ 0,1\}$. Another reasonable assumption would be that neighbouring pixels should more often than not belong to the same image segment. We can incorporate this assumption by constructing a pair-wise potential that penalizes neighbours who do not have the same label. The pair-wise potentials are also known as \textit{smoothness} terms since they
allow us to restrict the space of possible configurations such that only configurations with a certain smoothness criterion are considered. One straightforward modelling choice would be to penalize neighbouring  pixels that do not share the same label. We can model this assumption with the following potential
\begin{align}\label{E:EPDiff} 
\psi_{ij}(\alpha, \beta)= \lambda_{ij} \delta_{ij} (\alpha, \beta) 
\end{align}
such that
\begin{align}\label{E:EPDiff} 
\delta_{ij}(\alpha, \beta) = 
     \begin{cases}
       1 &\quad\text{if }  \alpha  \neq \beta    \\
       0  &\quad\text{otherwise}\\
     \end{cases}
\end{align}
where $\lambda_{ij}$ is a hyperparameter specifying how much the pair of neighbours $(i,j)$ are penalized for not sharing labels. If we fix $\lambda_{ij}$ for all pairs, we get the following non-linear energy 
\begin{align}\label{E:EPDiff} 
E =\sum_{i \in V} \left( \frac{y_i-\hat{\mu}_\alpha}{ \hat{\sigma}_\alpha}   \right)^2+  \sum_{i \in V}  \sum_{ j \in N(i)}  \lambda_{ij} \delta_{ij}(\alpha, \beta)
\end{align}
which we can then linearize following the steps in Section 4.4.5.


%\begin{figure}[h]
  %\begin{center}
   % \subfigure[Original image]{\label{fig:edge-a}\includegraphics[scale=0.5]{../figures/fig1}}
   % \subfigure[Original image with added noise]{\label{fig:edge-b}\includegraphics[scale=0.5]{../figures/fig2}} 
    %    \subfigure[Segmentation of the original image]{\label{fig:edge-b}\includegraphics[scale=0.5]{../figures/fig3}}
    %\subfigure[Segmentation of the noisy image]{\label{fig:edge-c}\includegraphics[scale=0.5]{../figures/fig4}}
   
  %\end{center}
  %\caption{  $\;\;$  Images c and d are binary segmentations of images a and b respectively. Both c and b were obtained by minimizing the energy function in equation 5.3.8}
  %\label{fig:edge}
%\end{figure}




\subsection{MRFs in Image Registration}

Reverting to our discretized map notation of an image, i.e. $I: \Omega \to \R$ where $\Omega$ is a discretized image domain. We now consider two images, $I$ and $T$, and want to retrieve a deformation that aligns them by warping one (the source image) onto the other (the target image). Before modelling such a problem with an MRF we first need to specify a deformation model and apply some discretization. We can express a general deformation in the following way
\begin{align}\label{E:EPDiff} 
\phi (\mathbf{x}) = \mathbf{x} +T(\mathbf{x} ) \;\;\;\; \text{where} \;\; T(\mathbf{x} ) =\sum\limits_{\mathbf{y} \in \Omega - \{\mathbf{x} \} } \mathbf{w}(\| \mathbf{x} - \mathbf{y}\| ) \mathbf{d_y} + \mathbf{w}(0) \mathbf{d_x}\;.
\end{align}
Thus expressing the deformation as a translation by a displacement vector $\mathbf{T( \cdot )}$ which is the result of a linear combination of displacement vectors at each point in $\Omega$. $\mathbf{w}( \cdot ) $ works as a weighting function that accounts for the contribution of each displacement vector. Therefore, specifying a $\mathbf{d_y} \;\forall \mathbf{y} \in \Omega$ specifies a displacement vector field on the image domain $\Omega$.\\

Let's further consider a discretization of the deformation space $\mathbf{D} = \{ \mathbf{d^1,...,d^m}\}$, to turn this into a labelling problem, we can let $\alpha \in \{ 1,...,m\}$ indicate a particular displacement vector $\mathbf{d^\alpha} \in \mathbf{D}$. Now we can rewrite the deformation at a particular point as a function of the label at that same point as follows
\begin{align}\label{E:EPDiff} 
\phi (\mathbf{x}, \alpha ) =\mathbf{x} + \sum\limits_{\mathbf{y} \in \Omega - \{\mathbf{x} \} } \mathbf{w}(\| \mathbf{x}- \mathbf{y}\| ) \mathbf{d_y} + \mathbf{w}(0) \mathbf{d_x^\alpha}\;.
\end{align}
We could consider a unitary potential that is a weighted squared $L^2-norm$ between the target and source image.
\begin{align}\label{E:EPDiff} 
\psi_i(\alpha)= \sum\limits_{\mathbf{y} \in \Omega }\kappa(x_i,\mathbf{y}) \| T(x_i)-I\circ \phi( x_i, \alpha)\|_{L^2}^2 
\end{align}
where $ \kappa( \cdot, \cdot )$ is some kernel distributing the error over the whole image domain. A reasonable assumption would be that the resulting labelling assigns similar displacement vectors to neighbouring labels; this would allow us to retrieve a smooth deformation. More formally, we can incorporate this assumption in the pair-wise potential with the following regularization
\begin{align}\label{E:EPDiff} 
\psi_{ij}(\alpha, \beta)= \lambda \| \mathbf{d}^\alpha_i  - \mathbf{d}^\beta_j  \|
\end{align}\\
where $ \lambda$ is a hyperparameter. Putting all of this together gives us the following energy
\begin{align}\label{E:EPDiff} 
E=\sum_{i \in V}  \sum\limits_{\mathbf{y} \in \Omega }\kappa(x_i,\mathbf{y}) \| T(x_i)-I\circ \phi( x_i, \alpha)\|^2_{L^2}+ \lambda \sum_{i \in V} \sum_{j  \in N(i)} \| \mathbf{d}^\alpha_i  - \mathbf{d}^\beta_j  \|\;.
\end{align}\\


%\begin{figure}[htp]
 % \begin{center}
   % \subfigure[Source image]{\label{fig:edge-a}\includegraphics[scale=0.5]{../figures/R1}}
   % \subfigure[Moving image]{\label{fig:edge-b}\includegraphics[scale=0.5]{../figures/R2}} 
     %   \subfigure[Deformed moving image]{\label{fig:edge-b}\includegraphics[scale=0.5]{../figures/R3}}

 % \end{center}
  %\caption{  $\;\;$Non-rigid registration between images a and b}
  %\label{fig:edge}
%\end{figure}


\section{Initial Experiments}

Our work initially considered an MRF-based simultaneous image registration and image segmentation framework in which MRF inference was done through constrained linear programs implemented using GEKKO [\citenum{beal2018gekko}], a Python optimization package. The implementation of the registration component, in particular, was found to be difficult and slow to converge. The latter limitation was crucial since an iterative approach to registration and segmentation would have to perform reasonably fast to justify combining both these processes. Moreover, though we could put hard constraints on the optimization in order to guarantee a diffeomorphic registration, it was unclear to us how such deformations would compare to LDDMM which is considered the state-of-the-art for registration [\citenum{klein2009evaluation}]. With the emergence of open-source deep learning registration frameworks, and in particular the recent availability of the mermaid package that efficiently implements LDDMM through automatic differentiation, we decided to take a different approach and considered a simultaneous registration and segmentation approach with a deep learning registration approach. The initial GEKKO-based MRF optimization took hours to converge and our current deep learning implementation converges under minutes. 



