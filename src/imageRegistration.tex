







In this chapter, we introduce the general theory of image registration and its applications. As previously mentioned, image registration is the process of geometrically aligning two or more images. Once the substructures are aligned the set of images is said to be registered and the images are now in spatial correspondence. To achieve this correspondence one of the images is held fixed while all other images are \textit{warped} such that they align with the fixed image. The fixed image is called the \textit{target, reference} or \textit{template} image and the warped images are called the \textit{source} or \textit{moving } images. Without loss of generality, we will consider the registration of two images, one fixed and one moving. Before registration, we need to specify a spatial transformation model and a method of quantifying image misalignment.



\section{Spatial Transformation Between Image Domains}

Throughout this thesis, we will assume that images being registered have been affinely co-registered to a common space, $\Omega$, beforehand. This image preprocessing step is typical in registration procedures since it allows us to omit large affine transformations  at registration time and also allows us to assume that the deformations are mapping between the same common space. A spatial transformation of an image is a mapping from the image domain to itself.

\begin{align}\label{E2-trans} 
\phi : \Omega \to \Omega
\end{align}
With this formulation, a transformed moving image is represented by the composition $ I \circ \phi$ which is also known as the pull-back of $ I$ by $\phi$. Since $\phi$ is a transformation of the image domain $\Omega$, it can alter quantities—such as intensities—defined on $\Omega$. If $\phi$ is bijective it can be viewed as a change of coordinates from source image coordinates to template coordinates or more generally viewed as an image transformation that changes the appearance of a source image, into the appearance of the target image. Consider the source and target images in Figure 3.4, where we are looking to warp an image of a square into an image of a circle. Subfigure 3.4 (e) shows the registered pull-back image, i.e. $ I \circ \phi$ and subfigures (e) and( f) demonstrate the corresponding warped coordinates assuming we started with a regular grid coordinate system.





\begin{figure}[H]
  \centering

     \subcaptionbox{(a) Identity }{\label{fig:square-warped-a}\includegraphics[width=.2\textwidth ]{../figures/reg/id}}\hfill%
     \subcaptionbox{(b) Affine (rigid)}{\label{fig:square-warped-a}\includegraphics[width=.2\textwidth ]{../figures/reg/affine-rigid}}\hfill%
     \subcaptionbox{(c) Affine (non-rigid)}{\label{fig:square-warped-a}\includegraphics[width=.2\textwidth ]{../figures/reg/affine-nonrigid}}\hfill%
     \subcaptionbox{(d) Non-linear}{\label{fig:square-warped-a}\includegraphics[width=.2\textwidth ]{../figures/reg/deformable}} 

     
     \subcaptionbox{(e) Identity }{\label{fig:square-warped-a}\includegraphics[width=.2\textwidth ]{../figures/reg/id2}} \hfill%
     \subcaptionbox{(f) Affine (rigid)}{\label{fig:square-warped-a}\includegraphics[width=.2\textwidth ]{../figures/reg/affine-rigid2}} \hfill%
     \subcaptionbox{(g) Affine (non-rigid)}{\label{fig:square-warped-a}\includegraphics[width=.2\textwidth ]{../figures/reg/affine-nonrigid2}} \hfill%
     \subcaptionbox{(h) Non-linear}{\label{fig:square-warped-a}\includegraphics[width=.2\textwidth ]{../figures/reg/deformable2}} 

  \caption{ Different types of image spatial transformations. Transformations (a)-(c) are linear and (d) is non-linear. }
  \label{fig:square-I-T}
\end{figure}







\subsection{Affine Registration} 

Affine registration refers to the case where the transformation space is restricted to affine transformations, more formally $\phi$ takes the following form

\begin{align}\label{E4-affine} 
\phi ( \mathbf{x} ) = Q\mathbf{x} + \mathbf{q} 
\end{align}
where $\mathbf{x} \in \Omega$, $Q$ is a $d$ by $d $ matrix and $\mathbf{q}$ is a $d$-dimensional vector. This class of transformations can adequately align a set of images when the misalignment between them can be modelled as an affine transformation such as a translation, rotation, scaling, shearing, reflection or any combination of them. Moreover, from an engineering point of view, affine transformations are relatively easy to implement due to their relatively low dimensional parametrization and relative ease of imposing sufficient invertibility conditions. 


\subsection{Non-Linear Registration}

Non-linear registration (or more correctly, non-affine registration) allows transformations that are not affine. The class of admissible transformations is typically very high-dimensional, often infinite-dimensional, making these registration schemes much more complicated and computationally demanding. We now outline several such approaches, focussing on the transformation classes. 

\subsubsection{Low-Frequency Basis Functions}

We can start by considering transformation classes that can model global non-linear variability between images. We can parametrize these  deformations by formulating them as linear combinations of low-frequency basis functions such as Fourier bases, sine and cosine transform basis functions, B-splines and piecewise affine or trilinear basis functions [\citenum{ashburner2003spatial}, \citenum{ashburner1999nonlinear} ]. Such a transformation model has the following form


\begin{align}\label{EE5-basis} 
\phi ( \mathbf{x}_i ) = \mathbf{x}_i + \limits \sum\limits_{j=1}^p \lambda_{ij} b_j( \mathbf{x}_i ) 
\end{align}
where  $\mathbf{x}_i$ is the $i^{th}$ entry of vector $\mathbf{x} \in \Omega$ and $b_j$ is the $j^{th}$ basis function. Under this setting, deformations of $d$-dimensional images can be parametrized by the following matrix



\begin{align}\label{E6-basis-param} 
\Lambda = 
\begin{bmatrix}
    \lambda_{11} & \dots  & \lambda_{1p} \\
    \vdots & \ddots & \vdots \\
     \lambda_{d1} & \dots  & \lambda_{dp}
\end{bmatrix}
\end{align}
where $p$ is the number of distinct basis functions. The parameter matrix $\Lambda$ is chosen such that it minimizes an objective function, also known as the \textit{energy}. The term energy comes from Elastic Registration Theory where the fixed and moving images are modelled as a continuous dense elastic plane sheet and under this perspective, Elasticity Theory is used to register the images. In particular, the registration problem is seen as a balancing of internal and external forces (image intensity differences) acting on elastic objects and the cost function is synonymous with the potential energy of this system; when the internal and external forces are balanced, the registration is complete [\citenum{crum2004non}]. 


\subsubsection{Radial Basis Functions}
%[\citenum{zitova2003image}]

The linear elasticity assumption in the previous section allows for global non-linear deformations but doesn't allow for highly localized deformations. This is in part because there is nothing local about the parametrization; the matrix $\Lambda$ is defined globally and is fixed over the entire image domain. Starting with the transformation model in Equation 3.1.3 we can define a transformation model that can handle localized deformations. First, we choose a countable set of points from $\Omega$, let's call this set of points $P \subset \Omega$. These points will allow us to define localized deformations by transforming these points and interpolating the deformation field across all of $\Omega$.  The points in $P$ are also called \textit{control points} or \textit{landmarks}. For any given point in the image domain, the deformation field will be interpolated in a way that considers the point's proximity to the control points. The distance between a given point and a control point will be computed through a radial basis function and the transformation model has the following form
 


\begin{align}\label{E7-radial} 
\phi ( \mathbf{x}_i ) = \mathbf{x}_i + \limits \sum\limits_{j=1}^p \lambda_{ij} b_j( \mathbf{x}_i ) + \limits \sum\limits_{\mathbf{p} \in P} \alpha_{\mathbf{p}} \kappa( \|\mathbf{x} - \mathbf{p}\| ) \;.
\end{align}
Note that Equation \ref{E7-radial}  is similar to 3.1.3 but with an extra term. $\mathbf{p}$ is a control point, $\alpha_{\mathbf{p}}$ is a parametric coefficient and $\kappa$ is the radial basis function. Typically under this transformation model, the basis functions are polynomials up to a specified degree and $\kappa$ is a Gaussian kernel or any positive-definite kernel. The number of control points and how they are distributed introduce an added level of complexity in registration schemes that use this class of transformation models. Since the control points introduce localization, they should be well distributed across the image domain $\Omega$, if localized deformations are to be expected across the image domain and if not, more control points can be placed in areas of high localized deformation. Moreover, the higher the number of control points the higher the dimension of the parametrization.  




\subsubsection{Large Deformation Diffeomorphic Metric Mapping}
Though the radial basis based transformation models allow for some level of localized deformation they are not a very principled way of achieving this end. This is due to the fact that under that transformation model, one cannot give sufficient conditions to guarantee desirable properties of the deformation field. By carefully sampling control points and regularizing the deformation field we can guarantee the invertibility of the resulting deformation [\citenum{rueckert2006diffeomorphic}] but this is not a simple task and it requires hard constraints that might compromise registration performance. Moreover, regularizing harshly under the basis function transformation models yields globally smooth deformations which result in small local deformations. This motivates the desire for a principled registration approach that retrieves transformations with attractive theoretical guarantees such as invertibility and topological invariance, while also performing well by allowing large deformations and high localization. The Large Deformation Diffeomorphic Metric Mapping (LDDMM) framework is such a framework. In LDDMM, the registration problem corresponds to a variational problem that finds a geodesic (shortest path) on the Riemannian manifold of diffeomorphisms on $\Omega$, i.e. $\mathbf{Diff}(\Omega)$. It's not self-evident how one can optimize over this group; one approach is to introduce a time variable $t$ and solve a system of partial differential equations. More specifically, it can be shown that one way to generate deformations from $\mathbf{Diff}(\Omega)$ is to solve the following non-stationary transport equation



\begin{align}\label{E8-fluid} 
\frac{ \partial \;  \phi(\mathbf{x} , t)}{ \partial t} &= v ( \phi(  \mathbf{x},t), t ) \;\; \text{such that } \;\;  \phi( \mathbf{x}, 0) = Id \;\; \text{and} \;\; t \in [0,1]
\end{align}
where $v ( \phi(  \mathbf{x},t), t )$ is a non-stationary smooth velocity vector field belonging to a Hilbert space $\mathcal{H}$. Under this setting, the final diffeomorphism is obtained through the following time-integration 

\begin{align}\label{E8-fluid} 
\phi( \mathbf{x}, 1) = \int \limits_0^1   v ( \phi(  \mathbf{x},t), t )dt + Id\;.
\end{align}
This integral describes a path on $\mathbf{Diff}(\Omega)$ that starts at $ Id$ and ends at $\phi( \mathbf{x}, 1) $. A technique known as \textit{geodesic shooting} [\citenum{younes2009evolutions} ] exploits the fact that the time-dependent velocity field, as a solution to the first-order PDE in Equation 3.1.6, is uniquely specified by its initial velocity i.e. $v(Id, 0)$. Intuitively,  geodesic shooting is analogous to repeatedly adjusting the angle of a cannon in order to specify a projectile trajectory. Restricting our problem to initial conditions significantly reduces the space-time algorithmic complexity during runtime since we would not have to compute and store an entire time series of velocity fields  [\citenum{sotiras2013deformable} ,\citenum{blaiotta2018generative} , \citenum{ashburner2007fast} ,  \citenum{miller2002metrics}].







\begin{figure}[H]
  \centering
     \subcaptionbox{ (a) Source ($I$) }{\label{fig:square-warped-a}\includegraphics[width=.23\textwidth ]  {../figures/warped/source}    } 
      \subcaptionbox{ (b) Target ($T$)  }{\label{fig:square-warped-a}\includegraphics[width=.23\textwidth ]  {../figures/warped/target}    } 
            
       \subcaptionbox{ (c)Identity map ($Id$) }{\label{fig:square-warped-a}\includegraphics[width=.23\textwidth ]  {../figures/warped/reg_grid}    }      
     \subcaptionbox{ (d) Source ($I \circ Id$)}{\label{fig:square-warped-a}\includegraphics[width=.23\textwidth ]   {../figures/warped/I}    } 
       \subcaptionbox{ (e) Source ($I \circ \phi$)}{\label{fig:square-warped-a}\includegraphics[width=.23\textwidth ]   {../figures/warped/IoPhi}    } 
       \subcaptionbox{ (f) Warp ($\phi$) }{\label{fig:square-warped-a}\includegraphics[width=.23\textwidth ]   {../figures/warped/warp_grid}    } 

  \caption{ Deformable registration. Source and target images are to be registered such that the  image of a square is warped onto an image of a circle along with the source images with superimposed coordinates.}\label{fig:warp}
\end{figure}






\section{Quantifying Image Alignment}

If we formalize the misalignment as some real-valued functional $E$ that acts on the warped image  $ I \circ \phi$ and target image $J$, we can then conceptualize the image registration problem as the following abstract optimization problem


\begin{align}\label{E3-opt} 
\phi^* = \argmin_{\phi} E [  I \circ \phi, J ]
\end{align}
where  $\phi^* $ is an optimal transformation that aligns the two images. The quantity $E$ is called an \textit{energy function}, \textit{cost function} or a \textit{ similarity measure}. The latter is potentially misleading since we are minimizing, so we really mean $E$ is a \textit{dissimilarity} measure; nonetheless the terminology is common. Similarity measures can broadly be categorized into two types, point-wise measures and global measures. For simplicity, In this section we will assume that the image domain $\Omega$ is discretized and therefore the similarity measures will be defined such that we sum over elements of $\Omega$ rather than integrating over them.


\subsection{Point-wise Similarity Measures}
Point-wise similarity measures compute the similarity of two images using point-wise intensity values—that is, intensities at individual pixels. These similarity measures are effective when the assumption that the intensity distribution is similar across images holds as in the case of images with the same modalities. Two common point-wise similarity measures are the normalized sum of squared differences (SSD) and the normalized sum of absolute differences (SAD) and they have the following forms





\begin{align}\label{E9-SSD} 
E_{SSD} \left(I,J \right)  = \frac{1}{ |\Omega |}  \sum\limits_{  \mathbf{x} \in \Omega }  \left(   I(\mathbf{x}) - J(\mathbf{x})    \right)^2
\end{align}
and
\begin{align}\label{E10-SAD} 
E_{SAD} \left(I,J \right)  = \frac{1}{ |\Omega |}  \sum\limits_{  \mathbf{x} \in \Omega }  |  I(\mathbf{x}) - J(\mathbf{x}) |\;.
\end{align}
These two similarity measures behave similarly and are easy to implement and interpret. SAD is slightly more robust to outlying intensities, while SSD over-penalizes them [\citenum{glocker2011random}].


\subsection{Global Similarity Measures}

We can derive more sophisticated similarity measures by modelling image intensities as a random sample coming from some intensity probability distribution. Under this modelling scheme, we can compute statistics for the entire image, which allows us to construct similarity measures that account for entire image regions as opposed to just pixel point-wise differences. One such measure is the correlation coefficient (CC) of two images and is calculated as follows
\begin{align}\label{E10-SAD} 
E_{CC} \left(I,J \right)  = \frac{     \sum\limits_{  \mathbf{x} \in \Omega }  \left(   I(\mathbf{x}) - \mu_I    \right)     \left(   J(\mathbf{x}) - \mu_J    \right)    }{   \sqrt{\sum\limits_{  \mathbf{x} \in \Omega }  \left(   I(\mathbf{x}) - \mu_I    \right)^2}             \sqrt{\sum\limits_{  \mathbf{x} \in \Omega }  \left(   J(\mathbf{x}) - \mu_J    \right)^2}            }
\end{align}
such that 
\begin{align}\label{E11-mean} 
\mu_I  = \frac{1}{  |\Omega| }  \sum\limits_{  \mathbf{x} \in \Omega }  I( \mathbf{x}) \;\;\; \text{ and }  \;\;\; \mu_J = \frac{1}{  |\Omega| }  \sum\limits_{  \mathbf{x} \in \Omega }  J( \mathbf{x}) 
\end{align}
where $\mu_I$ and $\mu_J$ are intensity means for images $I$ and $J$ respectively. $E_{CC}$ is a measure of the linear relationship between the image intensities and since we are considering dissimilarity, the objective function would be $E = 1-E_{CC}$.\\

Using information-theoretic measures we can construct similarity measures that are able to model more complex statistical relationships between image intensities; one such measure is the \textit{mutual information} (MI) of two images. We must first estimate the intensity probability distribution for each image using the image intensities as a random sample. The Statistics literature gives many ways to estimate probability mass functions (PMFs) from finite samples. With these PMFs, the Shannon marginal entropy is defined as follows
\begin{align}\label{E13-entropy-1} 
 \mathbf{H}(I)  = -  \sum\limits_{  \mathbf{x} \in \Omega } p(\mathbf{x}) \log p(\mathbf{x} ) 
\end{align}
where  $ p(\cdot ) $ is the estimated marginal intensity distribution for image an $I$. The Shannon joint entropy is defined as follows
\begin{align}\label{E14-entropy-2} 
 \mathbf{H}(I, J)  = -  \sum\limits_{  \mathbf{x} \in \Omega } \sum\limits_{  \mathbf{y} \in \Omega }  p(\mathbf{x}, \mathbf{y}) \log p(\mathbf{x}, \mathbf{y}) 
\end{align}
where  $ p(\cdot, \cdot  ) $ is the estimated joint intensity distribution for images $I$ and $J$. These entropies are statistical measures of \textit{information} or \textit{disorder}. The MI of two images is defined as follows
\begin{align}\label{E12-MI} 
E_{MI} \left(I,J \right) = \mathbf{H}(I) + \mathbf{H}(J) - \mathbf{H}(I,J)\;.
\end{align}
A normalized variant of MI is the entropy correlation coefficient (ECC)
\begin{align}\label{E13-ECC} 
E_{ECC} \left(I,J \right) = 2  - \frac{2\mathbf{H}(I,J)}{ \mathbf{H}(I)+ \mathbf{H}(J)}\;.
\end{align}
These measures work particularly well when doing multi-modal registration when the intensities across images are assumed to come from very different distributions. 

\subsection{Regularized Similarity}
So far we've only considered cost functions that only account for the similarity between the moving image and the target image, however, it would be desirable to introduce another term that penalizes deformations with undesirable properties, this is called regularizing. Without regularization, it would be possible to introduce unnecessary  deformations  that  only  reduce  the  cost function  by  a small amount [\citenum{ashburner1999nonlinear}]. Regularizing allows us to define a term that  restricts the solution space and that is independent of the image data. To do so, we add a term to the optimization problem in Equation 3.2.1 as follows
\begin{align}\label{E3-opt} 
\phi^* = \argmin_{\phi} E_{data} [  I \circ \phi, J ] + E_{reg} [   \phi ]
\end{align}
where $E_{reg}$ is the regularizer term. In this section, we revert back to the continuous definition of the image domain where $\Omega$ is a compact and simply connected subset of $\mathbb{R}^d$. To present concrete examples of regularizations, suppose we are under a deformation model that can be expressed as the following deformation field
\begin{align}\label{E5-basis} 
\phi ( \mathbf{x}) = \mathbf{x} -  \mathbf{u}  \;.
\end{align}
This transformation model allows for many deformations that might be undesirable and so regularization is necessary if one hopes to retrieve a plausible deformation. Three common regularization schemes are based on the minimization of the \textit{membrane}, \textit{bending}, \textit{linear elastic} energies and have the following form




\begin{align}\label{E5-basis} 
&E_{membrane} [   \phi ] = \int_{\Omega}    \sum \limits_{j=1}^{d}   \sum \limits_{i=1}^{d} \left( \frac{\partial   \mathbf{u}_j      }{\partial \mathbf{x}_i } \right)^2   d\mathbf{x}     \\
&E_{bending} [   \phi ] = \int_{\Omega} \sum \limits_{k=1}^{d} \sum \limits_{j=1}^{d}   \sum \limits_{i=1}^{d} \left( \frac{\partial^2   \mathbf{u}_k      }{\partial \mathbf{x}_i    \mathbf{x}_j  } \right)^2   d \mathbf{x}\\
&E_{elastic} [   \phi ] = \int_{\Omega}  \sum \limits_{j=1}^{d}   \sum \limits_{i=1}^{d}  \frac{\lambda_o}{2}  \left(   \frac{\partial   \mathbf{u}_i     }{\partial     \mathbf{x}_i  }      \right)\left(   \frac{\partial   \mathbf{u}_j     }{\partial     \mathbf{x}_j  }      \right) + \frac{\mu_o}{4}  \left( \frac{\partial   \mathbf{u}_i     }{\partial     \mathbf{x}_j  }   + \frac{\partial   \mathbf{u}_j     }{\partial     \mathbf{x}_i }  \right)^2  d\mathbf{x}
\end{align}
where $d\mathbf{x} = d\mathbf{x}_1\dots d\mathbf{x}_d$. The membrane, bending and linear elastic energies are based on the Laplacian, Hessian and Jacobian of the deformation fields respectively [\citenum{ashburner1999nonlinear}] and $\lambda_o$ and $\mu_o$ are Lamé elastic material constants [\citenum{christensen1996deformable}].\\

Under the LDDMM diffeomorphic transformation model from Section 3.1.2 we can regularize the corresponding velocity vector field to be as smooth as possible. Assuming the time-dependent velocity vector field $v$ (from equation 3.1.6) belongs to a Reproducing Kernel Hilbert space (RKHS) $\mathcal{H}$ with norm $\|  \cdot  \|_{\mathcal{H}}$ we can define the following regularization term



\begin{align}\label{E5-basis} 
E_{velocity} [   v( \cdot, 0) ] = \int_{\Omega} \int \limits_0^1  \|   v(\phi ( \mathbf{x}, t), t)   \|^2_{\mathcal{H}}  dt d\mathbf{x}
\end{align}
and the choice of reproducing kernel associated with $\mathcal{H}$ allows us to model spatial regularizations with different characteristics [\citenum{sotiras2013deformable}]; this regularizer penalizes diffeomorphisms that are far from the identity.


\subsection{Generative Model Perspective}
We now explain how the dissimilarity measures (\textit{energies}) defined above can be viewed as the negative log of a well-defined probability distribution over a discretized space of deformations. We will borrow the idea of a \textit{Gibbs distribution} (also known as a \textit{Boltzmann distribution}) from statistical physics [\citenum{risholm2010bayesian}]. In particular, a Gibbs measure can be used to relate the \textit{probability} of the state of a system to its \textit{energy} [\citenum{risholm2010bayesian}]; the underlying idea is that the states with the lesser energies are the most probable. Using this idea, we can model the similarity measures in Section 3.2 as the energy (or Hamiltonian) of a system, where the state space of a system corresponds to the space of deformations. To give a concrete example, we will consider the case where the deformations come from the group $\mathbf{Diff}(\Omega)$. We first note that $E_{reg}$ can be understood as a map
\begin{align}\label{E14-LH} 
E_{reg} : \mathbf{Diff}(\Omega) \to [0, \infty)
\end{align}
and we consider a Gibbs measure $\mu$ on $\mathbf{Diff}(\Omega)$ and a random variable $\Phi$ with support  $\mathbf{Diff}(\Omega)$ and under this abstract setting we can define a probability density function with the following form
\begin{align}\label{E14-LH} 
\mathbb{P} (\Phi = \phi ) = \frac{1}{ Z_{reg} (\beta)   }     \exp \big\{  -  \beta E_{reg} [  \phi ]   \big\} 
\end{align}
where $\beta$ is a free parameter and $Z_{reg}(\beta)$ is partition function corresponding to a normalizing constant with the following form 
\begin{align}\label{E14-LH} 
Z_{reg}(\beta) = \int  \exp \big\{   - \beta  E_{reg} [  \phi ]   \big\} d \mu(\phi) \;.
\end{align}
This distribution is well-define since  $E_{reg} \geq 0$ and $\mu < \infty$ are sufficient conditions for $Z_{reg}(\beta)$ to be finite. Moreover, given a deformation $\phi$, a source image $I$ which is a realization of a random variable $\mathbf{I}$ and a target image $T$ we can define the following likelihood function

\begin{align}\label{E14-LH} 
\mathcal{L}_I(\phi) &= \mathbb{P} ( \mathbf{I} = I |\; \phi , T )\\ 
&=  \frac{1}{Z_{data}} \exp \big\{  -  E_{data} [  I \circ \phi, T ]  \big\} \;.
\end{align}
where
\begin{align}\label{E14-LH} 
Z_{data} = \int_{\Omega}  \exp \big\{  -  E_{data} \big[  I \circ \phi (\mathbf{x}) , T(\mathbf{x} )\big]  \big\}  d \mathbf{x} \;.
\end{align}
Therefore the following optimization problem
\begin{align}\label{E3-opt} 
\phi_{MLE} = \argmax_{\phi} \mathcal{L}_I(\phi) 
\end{align}
is equivalent to the optimization problem in equation 3.2.1; that is, from a generative model perspective, an optimal deformation for registration is the Maximum Likelihood Estimate (MLE). If we include a regularized cost function we can then consider a Bayesian framework where the likelihood becomes a posterior distribution and the MLE becomes a Maximum a Posteriori (MAP) estimate, this can be achieved by considering the following posterior
\begin{align}\label{E14-LH} 
\mathbb{P} ( \Phi = \phi | \mathbf{I} = I, T ) =   \frac{      \exp \big\{  -  E_{data} [  I \circ \phi, T ] -  \beta E_{reg} [  \phi ]   \big\} }{  Z_{data} Z_{reg} (\beta)p(I)}\;.
\end{align}




