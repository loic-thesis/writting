%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This is the file UOthesis.cls
%
% Inspired by  MPLETTER.CLS  from Guide to Latex, 4th edition
% by H. Kopka and P. W. Daly
%
% Copyright 2008 by Benoit Dionne
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





\NeedsTeXFormat{LaTeX2e}[1996/06/01]
\ProvidesClass{UOthesis}[2015/10/10 v0.003 UO thesis style]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Packages required
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\RequirePackage{ifthen}
\RequirePackage{graphicx}
\usepackage{subcaption}
\usepackage{float}
\RequirePackage[usenames]{color}
\RequirePackage{amsmath}
\RequirePackage{amssymb}
\RequirePackage{amscd}
\RequirePackage{amsthm}
\RequirePackage{setspace}
\RequirePackage{bm}
\usepackage{tikz}
\usetikzlibrary{bayesnet}
\usepackage{bbm}





\usepackage{mathtools,algorithm}
\usepackage[noend]{algpseudocode}
\renewcommand{\algorithmicrequire}{\textbf{Input:}}
\renewcommand{\algorithmicensure}{\textbf{Output:}}


\DeclareMathOperator*{\argmin}{argmin}
\DeclareMathOperator*{\argmax}{argmax}


\usepackage{subfigure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Loic Defined Commands
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\R}{\mathbb{R}}
\newcommand{\C}{\mathbb{C}}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Possible options: all normal options for report style plus
% french, UOdraft, ...
%
% Default is oneside, english, 12pt,
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \newboolean{@twoside}
% \setboolean{@twoside}{false}
% \DeclareOption{twoside}{\setboolean{@twoside}{true}}

\newboolean{@UOdraft}
\setboolean{@UOdraft}{false}
\DeclareOption{UOdraft}{\setboolean{@UOdraft}{true}}

\newboolean{@french}
\setboolean{@french}{false}
\DeclareOption{french}{\setboolean{@french}{true}}

\newboolean{@NoTofC}
\setboolean{@NoTofC}{false}
\DeclareOption{NoTofC}{\setboolean{@NoTofC}{true}}

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{report}}
\ProcessOptions
\LoadClass{report}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Packages required for the PDF/A documents.
% The result is still not a fully compliant PDF/A document.
% The newest version of pdftex seems to produce by default what the
% following packages are supposed to do.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\RequirePackage{filecontents}
\RequirePackage[utf8x]{inputenc}
%\RequirePackage{colorprofiles}
\RequirePackage[T1]{fontenc}
\RequirePackage[a-3b]{pdfx}
%\RequirePackage[a-2b,mathxmp]{pdfx}[2018/12/22]

% hyperref is included by pdfx but pdfx doesn't complain if hyperref
% is not installed on the system.  It simply doesn't do anything.
% This line will produce the complaint if hyperref is not installed.
\RequirePackage{hyperref}
\hypersetup{pdfencoding=unicode}
% \hypersetup{pdfstartview=}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Structure of the theorems, propositions, ...
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




\ifthenelse{\boolean{@french}}{
  \newtheorem{thm}{Th\'eor\`eme}[section]
  \newtheorem{lem}[thm]{Lemme}
  \newtheorem{definition}[thm]{D\'efinition}
  \newtheorem{cor}[thm]{Corollaire}
  \newtheorem{prop}[thm]{Proposition}
}{
 

  
  \newtheorem{thm}{Theorem}[section]
  \newtheorem{lem}[thm]{Lemma}
  \newtheorem{definition}[thm]{Definition}
  \newtheorem{cor}[thm]{Corollary}
  \newtheorem{prop}[thm]{Proposition}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Counter for the equation number
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\setcounter{secnumdepth}{2}
\numberwithin{equation}{section}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Package to create a list of symbols.  With the option NoTofC, the
% section for the list of symbols isn't included in the table of
% Contents.  The default is that it is included in the list of
% contents.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\ifthenelse{\boolean{@NoTofC}}{
  \RequirePackage[refpage]{nomencl}
}{
  \RequirePackage[refpage,intoc]{nomencl}
}
\makenomenclature

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The index
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\RequirePackage{makeidx}
\makeindex

\newcommand{\PrintIndex}{
  \setcounter{chapter}{100}
  \addcontentsline{toc}{chapter}{\indexname}
  \printindex
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Table of contents, list of figures, list of tables, and list of
% symbols.
% If your thesis has section numbers larger than 9, many tables or
% many figures, a lot of pages, ... then some of the items on a line
% of the table of contents, the list of tables, the list of figures or
% the list of symbols may overlap.  The following commands should
% solve these problems.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\setlength{\nomitemsep}{-\parsep}

% List of symbols

\newlength{\UOspaceLabelSymbols}
\setlength{\UOspaceLabelSymbols}{1.5cm}
\renewcommand{\nomlabelwidth}{\UOspaceLabelSymbols}
\newcommand{\addSpaceLabelSymbols}[1]{
  \addtolength{\UOspaceLabelSymbols}{#1}
}

% To get more space between the dots of a dotted line.
\def\Dotfill{\xleaders\hbox to1em{\hfill.\hfill}\hfill}

\newlength{\UOspacePageSymbols}
\setlength{\UOspacePageSymbols}{1.5cm}
\renewcommand{\pagedeclaration}[1]{\ \Dotfill \makebox[\UOspacePageSymbols][r]{#1}}
\newcommand{\addSpacePageSymbols}[1]{
  \addtolength{\UOspacePageSymbols}{#1}
}

\newcommand{\PrintNomenclature}{
  \markboth{\MakeUppercase{\nomname}}{\MakeUppercase{\nomname}}
  \printnomenclature
}

% Table of contents

\newlength{\UOspaceContents}
\setlength{\UOspaceContents}{2em}
\renewcommand\l@section{\@dottedtocline{1}{\UOspaceContents}{3em}}
\newcommand{\addSpaceContents}[1]{
  \addtolength{\UOspaceContents}{#1}
}

\newlength{\UOspaceRightMargin}
\setlength{\UOspaceRightMargin}{4em}
\renewcommand\@tocrmarg{\UOspaceRightMargin}
\newcommand{\addSpaceRightMargin}[1]{
  \addtolength{\UOspaceRightMargin}{#1}
  \ifthenelse{\UOspaceRightMargin<\UOspacePageNumber}{
    \setlength{\UOspaceRightMargin}{\UOspacePageNumber}
  }{}
}

\newlength{\UOspacePageNumber}
\setlength{\UOspacePageNumber}{3em}
\renewcommand\@pnumwidth{\UOspacePageNumber}
\newcommand{\addSpacePageNumber}[1]{
  \addtolength{\UOspacePageNumber}{#1}
  \addtolength{\UOspaceRightMargin}{#1}
}

% \RequirePackage{tocloft}
% \newcommand{\addSpaceFigure}[1]{\addtolength{\cftfignumwidth}{#1}}
% \newcommand{\addSpaceTable}[1]{\addtolength{\cfttabnumwidth}{#1}}
% \cftsetpnumwidth{1.5cm}
% \cftsetrmarg{2.5cm}


% List of figures

\newlength{\UOspaceFigure}
\setlength{\UOspaceFigure}{2em}
\renewcommand\l@figure[2]{\@dottedtocline{1}{1.5em}{\UOspaceFigure}{#1}{#2}}
\newcommand{\addSpaceFigure}[1]{
  \addtolength{\UOspaceFigure}{#1}
}

\newcommand{\ListOfFigures}{
  \listoffigures
  \addcontentsline{toc}{chapter}{\listfigurename}
}

% List of tables

\newlength{\UOspaceTable}
\setlength{\UOspaceTable}{2em}
\renewcommand\l@table[2]{\@dottedtocline{1}{1.5em}{\UOspaceTable}{#1}{#2}}
\newcommand{\addSpaceTable}[1]{
  \addtolength{\UOspaceTable}{#1}
}

\newcommand{\ListOfTables}{
  \listoftables
  \addcontentsline{toc}{chapter}{\listtablename}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Table of contents, list of figures, list of tables, and list of
% symbols.
% In French and in English
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\ifthenelse{\boolean{@french}}{
  \renewcommand{\nomname}{Liste des symboles}
  \renewcommand{\contentsname}{Table des mati\`eres}
  \renewcommand{\listfigurename}{Liste des figures}
  \renewcommand{\listtablename}{Liste des tableaux}
}{
  \renewcommand{\nomname}{List of Symbols}
  \renewcommand{\contentsname}{Contents}
  \renewcommand{\listfigurename}{List of Figures}
  \renewcommand{\listtablename}{List of Tables}
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Page style
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\ifthenelse{\boolean{@UOdraft}}{
  \setboolean{@twoside}{true}
}{}

\raggedbottom
\ifthenelse{\boolean{@twoside}}{
  \RequirePackage[letterpaper,lmargin=1.1in,height=8.3in,width=6.4in,twoside,%
  top=1.5in]{geometry}
}{
  \RequirePackage[letterpaper,lmargin=1.1in,height=8.3in,width=6.4in,%
  top=1.5in]{geometry}
}
\setlength\parskip{0ex plus 0.3ex minus 0ex}
\setlength\parindent{2em}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%ORIGINAL
%\raggedbottom
%\ifthenelse{\boolean{@twoside}}{
 % \RequirePackage[letterpaper,lmargin=1.5in,height=8.3in,width=6in,twoside,%
  %top=1.5in]{geometry}
%}{
  %\RequirePackage[letterpaper,lmargin=1.5in,height=8.3in,width=6in,%
  %top=1.5in]{geometry}
%}
%\setlength\parskip{0ex plus 0.3ex minus 0ex}
%\setlength\parindent{2em}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% font for the fancy header
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \newcommand{\hdf}{\fontfamily{phv}\fontseries{b}\fontsize{10}{11}\selectfont}
\newcommand{\hdf}{\rmfamily \bfseries}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Header
%
% 1) the page number on the left followed by the title of the
%    chapter for the even numbered pages (left pages).
% 2) the page number on the right preceded by the title of the
%    section for the odd numbered pages (right pages).
% For a one-sided document, only 2 is applied.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\RequirePackage{fancyhdr}
\pagestyle{fancy}
\setlength{\headheight}{1.5\baselineskip}
\setlength{\headsep}{1.5\baselineskip}
\renewcommand{\chaptermark}[1]{\markboth{\MakeUppercase{\thechapter.\ #1}}{}}
\renewcommand{\sectionmark}[1]{\markright{\MakeUppercase{\thesection.\ #1}}}
% \renewcommand{\chaptermark}[1]{\markboth{\thechapter.\ #1}{}}
% \renewcommand{\sectionmark}[1]{\markright{\thesection.\ #1}}
\renewcommand{\headrulewidth}{0.5pt}
\renewcommand{\plainheadrulewidth}{0pt}
\fancyhf{}
\fancyhead[LE,RO]{\hdf \thepage}
\ifthenelse{\boolean{@twoside}}{
  \fancyhead[LO]{\hdf \rightmark}
}{
  \fancyhead[LO]{\hdf \leftmark}
}
\fancyhead[RE]{\hdf \leftmark}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% French option
%
% Il est possible de compiler un texte qui possede des accents, ...
% sans avoir a recourir aux symboles \'e, \"u, etc.
% En plus de permettre l'utilisation d'un editeur de texte normal,
% vous pouvez aussi utiliser votre "spell checker" prefere.
% Pour utiliser cette option, vous pourrier avoir a enlever le symbole
% de commentaire devant la ligne qui contient latin1 et ajouter le
% symbole de commentaire devant la ligne qui contient  utf8x (unicode) .
% Cela va dependre de l'environnement dans lequel votre editeur de textes
% fonctionne.
%
% WARNING: The packages  babel  and  xy-pic  are incompatible.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\ifthenelse{\boolean{@french}}{
  \RequirePackage[french]{babel}
  \RequirePackage{ucs}
  % \RequirePackage[T1]{fontenc}
  % \RequirePackage[utf8x]{inputenc}
  % \RequirePackage[latin1]{inputenc}
}{}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Footnotes with single line spacing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\let\UOtmpFN=\footnote
\renewcommand{\footnote}[1]{
  \begin{singlespace}
  \hspace*{-1em}\UOtmpFN{#1}
  \end{singlespace}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Caption with single line spacing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\RequirePackage[margin=0.05\textwidth]{caption}
\let\UOtmpCT=\caption
\renewcommand{\caption}[2][TTT]{
  \begin{singlespace}
  \ifthenelse{\equal{TTT}{#1}}{\UOtmpCT{#2}}{\UOtmpCT[#1]{#2}}
  \end{singlespace}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% New environment for chapter without number
%
% \nonumchapter{chapter_name}
% to produce the header of a chapter without a number;
% the chapter is still listed in the contents.
%
% \notachapter{chapter_name}
% to produce the header of a chapter without a number;
% the chapter is not listed in the contents.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\nonumchapter}[1]{
  \chapter*{#1 \@mkboth{\MakeUppercase{#1}}{\MakeUppercase{#1}}}
  \addcontentsline{toc}{chapter}{#1}
}

\newcommand{\notachapter}[1]{
  \chapter*{#1 \@mkboth{\MakeUppercase{#1}}{\MakeUppercase{#1}}}
}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Important variables supplied by the user
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\UO}{University of Ottawa}
\newcommand{\setUO}[1]{\renewcommand{\UO}{#1}}

\newcommand{\UOgradfac}{Faculty of Science}
\newcommand{\setUOgradfac}[1]{\renewcommand{\UOfac}{#1}}

\newcommand{\UOprogrfac}{Faculty of Science}
\newcommand{\setUOprogrfac}[1]{\renewcommand{\UOfac}{#1}}

\newcommand{\UOdept}{Department of Mathematics and Statistics}
\newcommand{\setUOdept}[1]{\renewcommand{\UOdept}{#1}}

\newcommand{\UOname}{}       % Full name of the candidate
\newcommand{\setUOname}[1]{\renewcommand{\UOname}{#1}}

\newcommand{\UOfdegree}{}    % Full title of the degree sought
\newcommand{\setUOfdegree}[1]{ \renewcommand{\UOfdegree}{#1} }

\newcommand{\UOdegree}{}     % Abbreviation of the title of the degree sought
\newcommand{\setUOdegree}[1]{ \renewcommand{\UOdegree}{#1} }

\newcommand{\UOtitlefootnote}{}   % title for the institute footnote
\newcommand{\setUOtitlefootnote}[1]{\renewcommand{\UOtitlefootnote}{#1}}

\newcommand{\UOcpryear}{}  % Year that should appear with the copyright notice
\newcommand{\setUOcpryear}[1]{\renewcommand{\UOcpryear}{#1}}

\newcommand{\UOtitle}{}    % The title of the thesis
\newcommand{\setUOtitle}[1]{\renewcommand{\UOtitle}{#1}}

% All the following variables are not used at the Univ. of Ottawa
\newcommand{\UOpdegree}{}     % Initial of the degrees own by the candidate
\newcommand{\setUOpdegree}[1]{\renewcommand{\UOpdegree}{#1}}

\newcommand{\UOdatedef}{}     % Date of the defense
\newcommand{\setUOdatedef}[1]{\renewcommand{\UOdatedef}{#1}}

\newcommand{\UOrefone}{}      % The first referee
\newcommand{\setUOrefone}[1]{ \renewcommand{\UOrefone}{#1} }

\newcommand{\UOreftwo}{}      % The second referee
\newcommand{\setUOreftwo}[1]{ \renewcommand{\UOreftwo}{#1} }

\newcommand{\UOrefthree}{}    % The third referee
\newcommand{\setUOrefthree}[1]{ \renewcommand{\UOrefthree}{#1} }

\newcommand{\UOreffour}{}     % The fourth referee
\newcommand{\setUOreffour}[1]{ \renewcommand{\UOreffour}{#1} }

\newcommand{\UOreffive}{}     % The fifth referee
\newcommand{\setUOreffive}[1]{ \renewcommand{\UOreffive}{#1} }

\newcommand{\UOextrefone}{}   % The first external referee
\newcommand{\setUOextrefone}[1]{ \renewcommand{\UOextrefone}{#1} }

\newcommand{\UOextreftwo}{}   % The second external referee
\newcommand{\setUOextreftwo}[1]{ \renewcommand{\UOextreftwo}{#1} }

\newcommand{\UOextrefthree}{} % The third external referee
\newcommand{\setUOextrefthree}[1]{ \renewcommand{\UOextrefthree}{#1} }

\newcommand{\UOsupone}{}      % The first supervisor
\newcommand{\setUOsupone}[1]{ \renewcommand{\UOsupone}{#1} }

\newcommand{\UOsuptwo}{}      % The second supervisor
\newcommand{\setUOsuptwo}[1]{ \renewcommand{\UOsuptwo}{#1} }

\newcommand{\UOsupthree}{}    % The third supervisor
\newcommand{\setUOsupthree}[1]{ \renewcommand{\UOsupthree}{#1} }

% If French option has been chosen
\ifthenelse{\boolean{@french}}{
  \renewcommand{\UO}{Universit\'e d'Ottawa}
  \renewcommand{\UOgradfac}{Facult\'e des sciences}
  \renewcommand{\UOprogrfac}{Facult\'e des sciences}
  \renewcommand{\UOdept}{D\'epartement de math\'ematiques et de statistique}
}{}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Short cut to define all the variables associated to the degree
% sought.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newboolean{UObbsc}
\setboolean{UObbsc}{false}
\newboolean{UObmsc}
\setboolean{UObmsc}{false}
\newboolean{UObphd}
\setboolean{UObphd}{false}
\newcommand{\UOthesisORproject}{}
\newcommand{\setUOthesisORproject}[1]{ \renewcommand{\UOthesisORproject}{#1} }
\newcommand{\msc}{
  \ifthenelse{\equal{\UOfdegree}{}}{
    \ifthenelse{\boolean{@french}}{
      \setUOfdegree{Ma\^{\i}trise \`es sciences Math\'ematiques et statistique}
      \setUOtitlefootnote{ma\^{\i}trise}
    }{
      \setUOfdegree{Master of Science Mathematics and Statistics}
      \setUOtitlefootnote{M.Sc.}
    }
  }{}
  \ifthenelse{\equal{\UOdegree}{}}{
    \setUOdegree{M.Sc.}
  }{}
  \setboolean{UObbsc}{false}
  \setboolean{UObmsc}{true}
  \setboolean{UObphd}{false}
  \ifthenelse{\boolean{@french}}{
    \setUOthesisORproject{Th\`ese soumisse en vue de l'obtention de la\\ \UOfdegree}
  }{
    \setUOthesisORproject{Thesis submitted in partial fulfillment of the requirements for the degree of\\ \UOfdegree}
  }
  % \ifthenelse{\boolean{@french}}{
  %   \setUOthesisORproject{Th\`ese soumisse \`a la \UOgradfac\ en vue de l'obtention de la\\ \UOfdegree}
  % }{
  %   \setUOthesisORproject{Thesis submitted to the \UOgradfac\ in partial fulfillment of the requirements for the degree of\\ \UOfdegree}
  % }
}
\newcommand{\phd}{
  \ifthenelse{\equal{\UOfdegree}{}}{
    \ifthenelse{\boolean{@french}}{
      \setUOfdegree{Doctorat en philosophie Math\'ematiques et statistique}
      \setUOtitlefootnote{doctorat}
    }{
      \setUOfdegree{Doctorate in Philosophy Mathematics and Statistics}
      \setUOtitlefootnote{Ph.D.}
    }
  }{}
  \ifthenelse{\equal{\UOdegree}{}}{
    \setUOdegree{Ph.D.}
  }{}
  \setboolean{UObbsc}{false}
  \setboolean{UObmsc}{false}
  \setboolean{UObphd}{true}
  \ifthenelse{\boolean{@french}}{
    \setUOthesisORproject{Th\`ese soumisse en vue de l'obtention du\\ \UOfdegree}
  }{
    \setUOthesisORproject{Thesis submitted in partial fulfillment of the requirements for the degree of\\ \UOfdegree}
  }
  % \ifthenelse{\boolean{@french}}{
  %   \setUOthesisORproject{Th\`ese soumisse \`a la \UOgradfac\ en vue de l'obtention du\\ \UOfdegree}
  % }{
  %   \setUOthesisORproject{Thesis submitted to the \UOgradfac\ in partial fulfillment of the requirements for the degree of\\ \UOfdegree}
  % }
}
\newcommand{\project}{
  \ifthenelse{\equal{\UOfdegree}{}}{
    \ifthenelse{\boolean{@french}}{
      \setUOfdegree{Ma\^{\i}trise \`es sciences Math\'ematiques et statistique}
      \setUOtitlefootnote{ma\^{\i}trise}
    }{
      \setUOfdegree{Master of Science Mathematics and Statistics}
      \setUOtitlefootnote{M.Sc.}
    }
  }{}
  \ifthenelse{\equal{\UOdegree}{}}{
    \setUOdegree{M.Sc.}
  }{}
  \setboolean{UObbsc}{false}
  \setboolean{UObmsc}{true}
  \setboolean{UObphd}{false}
  \ifthenelse{\boolean{@french}}{
    \setUOthesisORproject{Projet soumis en vue de l'obtention de la\\ \UOfdegree}
  }{
    \setUOthesisORproject{Project submitted in partial fulfillment of the requirements for the degree of\\ \UOfdegree}
  }
}
\newcommand{\uproject}{
  \ifthenelse{\equal{\UOfdegree}{}}{
    \ifthenelse{\boolean{@french}}{
      \setUOfdegree{Baccalaur\'eat}
      \setUOtitlefootnote{}
    }{
      \setUOfdegree{Bachelor}
      \setUOtitlefootnote{}
    }
  }{}
  \setboolean{UObbsc}{true}
  \setboolean{UObmsc}{false}
  \setboolean{UObphd}{false}
  \ifthenelse{\equal{\UOdegree}{}}{
    \setUOdegree{B.Sc.}
  }{}
  \ifthenelse{\boolean{@french}}{
    \setUOthesisORproject{Projet soumis en vue de l'obtention du \UOfdegree}
  }{
    \setUOthesisORproject{Project submitted in partial fulfillment of the requirements of the \UOfdegree}
  }
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Logo, title page, and signatures
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newboolean{UOblogo}
\setboolean{UOblogo}{true}
\newcommand{\NoLogo}{\setboolean{UOblogo}{false}}

\newboolean{UObtitlepage}
\setboolean{UObtitlepage}{true}
\newcommand{\NoTitlepage}{\setboolean{UObtitlepage}{false}}

\newboolean{UObsignatures}
\setboolean{UObsignatures}{true}
\newcommand{\NoSignatures}{\setboolean{UObsignatures}{false}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Options for the UOdraft.  Note that twoside mode is also set.
% This may save some trees.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\UOdraftname}{Draft}
\ifthenelse{\boolean{@french}}{
  \renewcommand{\UOdraftname}{Brouillon}
}{}

\ifthenelse{\boolean{@UOdraft}}{
  \renewcommand{\baselinestretch}{1}
  \NoLogo
  % \NoTitlepage
  \NoSignatures
  \fancyhf{}
  \fancyhead[LE,RO]{\hdf \thepage}
  \fancyhead[CO,CE]{\footnotesize\MakeUppercase{\UOdraftname -- \today}}
  \fancyhead[LO]{\hdf \rightmark}
  \fancyhead[RE]{\hdf \leftmark}
}{}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The page for the logo
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\UOlogoloc}{UOlogoBW}       % Location of the logo
\newcommand{\UOlogoopt}{}               % Options for includegraphics
\newcommand{\setUOlogoloc}[2]{
  \renewcommand{\UOlogoloc}{#1}
  \renewcommand{\UOlogoopt}{#2}
}

\newcommand{\UOlogopage}{
  \thispagestyle{empty}
  \begin{singlespace}
  \vspace*{\fill}
  \begin{center}
    \ifthenelse{\equal{\UOlogoopt}{}}{
      \includegraphics[width=1in]{\UOlogoloc}
    }{
      \includegraphics[\expandafter\noexpand\UOlogoopt]{\UOlogoloc}
    }
  \end{center}
  \end{singlespace}
  \newpage
  \ifthenelse{\boolean{@twoside}}{\thispagestyle{empty}\ \newpage}{}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The title page
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\UOtitlepage}{
  \thispagestyle{empty}
  \begin{singlespace}
  \ifthenelse{\boolean{@UOdraft}}{
    \begin{center} \Large\upshape{\UOdraftname}\\ \large \today \end{center}
  }{  }
  \vspace*{0.3in}
  \begin{sloppypar}
    \begin{center}
      \Large\upshape{\UOtitle}
    \end{center}
  \end{sloppypar}
  \vfill
  \begin{center}
    \large\rm \UOname
  \end{center}
  \vfill
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Loic: This next block of code is the footnote at the bottom of the tittle page, 
%		  I commented it out because I don't like how it looks, if I am required to 
%  		  include it later, just uncomment it. 
  		  
  		  
%   \begin{center}
  %   \ifthenelse{\boolean{UObbsc}}{
%       \UOthesisORproject
%     }{
 %      \ifthenelse{\boolean{@french}}{
 %        \UOthesisORproject\footnotemark\footnotetext{le programme de
 %          \UOtitlefootnote\ est un programme conjoint avec
 %          l'Universit\'e Carleton, administr\'e par l'Institut en
 %          math\'ematiques et en statistique d'Ottawa-Carleton}
 %      }{
 %       \UOthesisORproject\footnotemark\footnotetext{
 %       
 %      The \UOtitlefootnote\ 
 %        program is a joint program with Carleton University,
 %       administered by the Ottawa-Carleton Institute of Mathematics
 %       and Statistics
 %        		}
  %     }
%     }
%   \end{center}%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  
  
  
  
  \vfill
  \begin{center}
    \UOdept\\
    \UOprogrfac\\
    \UO
  \end{center}
  \vfill
  \begin{center}
    \copyright\ \UOname, Ottawa, Canada, \UOcpryear
  \end{center}
  \end{singlespace}
  \newpage
  \ifthenelse{\boolean{@twoside}}{\thispagestyle{empty}\ \newpage}{}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The page with the signatures     NOT USED AT UO
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newlength{\UOunderline}
\newcommand{\UOsignature}[2]{
  \settowidth{\UOunderline}{#1 (#2)}
  \noindent
  \underline{\hspace{\UOunderline}}\\#1 (#2)
  \\[1em]
}

\newcommand{\UOsuptitle}{Supervisor}
\newcommand{\UOreftitle}{Referee}
\newcommand{\UOextreftitle}{External Referee}
\ifthenelse{\boolean{@french}}{
  \renewcommand{\UOsuptitle}{Superviseur}
  \renewcommand{\UOreftitle}{\'Evaluateur}
  \renewcommand{\UOextreftitle}{\'Evaluateur externe}
}{}

\newcommand{\UOsignaturespage}{
  \thispagestyle{empty}
  \begin{singlespace}
    \@for\UOsuperV:=\UOsupone,\UOsuptwo,\UOsupthree\do{
      \ifthenelse{\equal{\UOsuperV}{}}{}{
        \UOsignature{\UOsuperV}{\UOsuptitle}
      }
    }
    \vfill
    \begin{sloppypar}
      \ifthenelse{\boolean{@french}}{
        Les signataires ci-dessous certifient qu'ils ont lu et
        recommand\'e la th\`ese intitul\'ee
        \linebreak[2] \mbox{\bf\UOtitle} \linebreak[2] par
        \mbox{\bf\UOname} \linebreak[2] \`a la \mbox{\UOgradfac}\ en vue de
        l'obtention
        \ifthenelse{\boolean{UObphd}}{
          du
        }{
          de la
        }
        \linebreak[2] \mbox{\bf\UOfdegree}. \\
      }{
        The undersigned hereby certify that they have read and
        recommend to the \mbox{\UOgradfac}\ for acceptance a thesis entitled
        \linebreak[2] \mbox{\bf\UOtitle} \linebreak[2] by
        \mbox{\bf\UOname} \linebreak[2] in partial fulfillment of
        the requirements for the degree of \linebreak[2]
        \mbox{\bf\UOfdegree}. \\
      }
    \end{sloppypar}
    \vfill
    \@for \UOextreF:=\UOextrefone,\UOextreftwo,\UOextrefthree \do{
      \ifthenelse{\equal{\UOextreF}{}}{}{
        \UOsignature{\UOextreF}{\UOextreftitle}
      }
    }
    \@for \UOreF:=\UOrefone,\UOreftwo,\UOrefthree,\UOreffour,\UOreffive \do{
      \ifthenelse{\equal{\UOreF}{}}{}{
        \UOsignature{\UOreF}{\UOreftitle}
      }
    }
  \end{singlespace}
  \begin{flushright}
    \ifthenelse{\boolean{@french}}{
      Date: \underbar{\ \UOdatedef\ }
    }{
      Dated: \underbar{\ \UOdatedef\ }
    }
  \end{flushright}
  \vfill
  \cleardoublepage
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The bibliography
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Hand made bibliography
\newenvironment{BasicBibliography}{
  \begin{thebibliography}{999}
    \ifthenelse{\boolean{@french}}{
      \addcontentsline{toc}{chapter}{Bibliographie}
    }{
      \addcontentsline{toc}{chapter}{Bibliography}
    }
  }{\end{thebibliography}}

% Bibliography made with BibTeX
% Include only the references in the bib file which are cited in the
% text.
\newcommand{\nostarBibTex}[1]{
  \bibliography{#1}
  \ifthenelse{\boolean{@french}}{
    \addcontentsline{toc}{chapter}{Bibliographie}
  }{
    \addcontentsline{toc}{chapter}{Bibliography}
  }
}

% Bibliography made with BibTeX
% Include all references in the bib file even if they are not cited in
% the text.
\newcommand{\starBibTex}[1]{
  \bibliographystyle{plain}
  \nocite{*}
  \bibliography{#1}
  \ifthenelse{\boolean{@french}}{
    \addcontentsline{toc}{chapter}{Bibliographie}
  }{
    \addcontentsline{toc}{chapter}{Bibliography}
  }
}

\makeatletter
\newcommand{\bibTexBibliography}{\@ifstar\starBibTex\nostarBibTex}
\makeatother

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create the preface of the thesis
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\preface}{
  \ifthenelse{\boolean{UOblogo}}{
    \UOlogopage
  }{}
  \pagenumbering{roman}
  \ifthenelse{\boolean{UObtitlepage}}{
    \UOtitlepage
  }{}
  \ifthenelse{\boolean{UObsignatures}}{
    \UOsignaturespage
  }{}
  \typeout{Univ. of Ottawa -- The preface has been added.}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The commands \caption and \footnote have been redefined to use only
% a single line spacing as required.
%
% The tabular and array environment MUST also use single line
% spacing.  For this purpose, use the following environment.
%
% \begin{singlespace}
%  your table, array, ...
% \end{singlespace}
%
% You may (and probably should) use the following command for floating
% figures and tables.
%
% \figcap[ ]{ \includegraphics{...} or other figure commands }{Full text
% for the caption}{Short description for the list of figures at the
% beginning of the thesis}{label_name for referencing with \ref{} }
%
% \tabcap[ ]{ \begin{tabular}{ccc} ... \end{tabular} }{Full text for the
% caption}{Short description for the list of tables at the beginning
% of the thesis}{label_name for referencing with \ref{} }
%
% The [ ] is optional.  The possible values are:
% [h]  <- Put the figure or table here in the text.
% [t]  <- Put the figure or table at the top of the page
% [b]  <- Put the figure or table at the bottom of the page
% [p]  <- Put the figure or table on a special page with only figures
%         or tables.
% You may also combine them; for instance [tb] tells LaTeX to try to
% put the table on the top of the page and if this fail on the bottom
% of the page.  LaTeX is not forced to respect your choice.
% Some implementations of LaTeX have the additional option H to force
% LaTeX to put the figure or table where you want.  The standard LaTeX
% distribution does not have it.
%
% See the template for examples.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\figcap}[5][TTT]{
  \ifthenelse{\equal{TTT}{#1}}{ \begin{figure} }{ \begin{figure}[#1] }
      \begin{center}
        #2
      \end{center}
      \caption[#4]{#3 \label{#5}}
    \end{figure}
  }

\newcommand{\tabcap}[5][TTT]{
  \ifthenelse{\equal{TTT}{#1}}{ \begin{table} }{ \begin{table}[#1] }
    \begin{center}
      \begin{singlespace}
      #2
      \end{singlespace}
    \end{center}
    \caption[#4]{#3 \label{#5}}
  \end{table}
}

\renewcommand{\qed}{\hfill \mbox{\raggedright \rule{0.4em}{0.8em}}\\[1em]}
\ifthenelse{\boolean{@french}}{
  \renewenvironment{proof}[1][TTT]{\noindent\textbf{D\'emonstration:}
    \ifthenelse{\equal{TTT}{#1}}{\hspace*{1em}}{({\bfseries #1}) \\ \noindent}
    \normalfont}{\qed}
}{
  \renewenvironment{proof}[1][TTT]{\noindent\textbf{Proof:}
    \ifthenelse{\equal{TTT}{#1}}{\hspace*{1em}}{({\bfseries #1}) \\ \noindent}
    \normalfont}{\qed}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% First commands to run after \begin{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\AtBeginDocument{
  \preface
}
