

Digital image segmentation can be broadly classified into two categories, manual segmentation and automated segmentation. In manual segmentation, a human manually annotates the different segments in a digital image, whereas in automated segmentation an automated algorithm segments the image. Manual segmentation comes with various challenges, we present three. Firstly, in many applications, the human who annotates the images needs to be a domain expert—this makes manual segmentation operationally challenging and costly. In medical neuroimaging, for instance, some of the common segmentation types are tissue type segmentation where brain tissue is categorized into three major tissue types, white matter (WM), grey matter (GM) and cerebrospinal fluid (CSF), see Figure 2.2. Manually annotating even one neuroimage volume of a patient would require a considerable amount of time and the expert knowledge of a radiologist. Secondly, the image processing task might require annotating an unmanageable quantity of images, on the order of hundreds or even thousands, which quickly becomes intractable when done manually. Thirdly, manual segmentations tend to vary greatly across annotators, even in the case where the annotators are domain experts [\citenum{patil2013medical}]. Moreover, certain scanners produce images that could be automatically segmented with relative ease. A CT neuroimage for instance has intensities with fixed physical correspondence, making automated segmentation a straightforward task. For those reasons and more, automated segmentation is preferred in practice. In this chapter, we will broadly categorise automated segmentation methods into three categories, generative model-based methods, deep learning methods and a broad category that contains methods that are not included in the first two. Conceptually, a segmentation $Z$ of the image $I$ can be represented as the following map
\begin{align}\label{E2.2-seg} 
Z : \Omega \to \{0,...,K-1\}
\end{align}
where $K$ is the number of distinct image segments. 


\begin{figure}[H]
  \centering
     \subcaptionbox{(a) Axial slice }{\label{fig:square-warped-a}\includegraphics[width=.25\textwidth ]{../figures/seg/seg1}} \hspace*{30px}
     \subcaptionbox{(b) Tissue segmentation}{\label{fig:square-warped-a}\includegraphics[width=.25\textwidth ]{../figures/seg/seg2}}
  \caption{A segmentation of an axial brain slice. Red, green and blue correspond to white matter, grey matter and cerebrospinal fluid respectively. (Figure taken from [\citenum{withey2008review}, Fig. 1])    }
  \label{fig:square-I-T}
\end{figure}





\section{Generative Model-Based Methods}

In generative model-based methods, the segmentation of an image is modelled as a statistical inference problem. More specifically, a generative model of the image is specified, in which the segmentation is a latent variable and segmenting an image corresponds to inferring this variable.  

\subsection{Univariate Models}

In a point-wise fashion, univariate methods model image intensities from different segments as sub-populations from a Finite Mixture Model (FMM) with different intensity distributions describing the data generating processes of different segments. Under this modelling approach image intensities belonging to specific segments are drawn from distinct mixtures. A special case of FMM that is often used in image segmentation is a Gaussian mixture model (GMM) where the mixture distributions are Gaussians [ \citenum{ashburner2005unified}, \citenum{blaiotta2018generative}, \citenum{farnoush2008image}]; we will use a GMM to demonstrate how a generative model can be used  to segment an image. The image is represented by a random variable $I$ with is a collection of independent random variables $I(\mathbf{x}) $ with support $\R$ and let $ Z(\mathbf{x}) $ be random variable with support $\{0,1\}^K $ representing the intensity value and mixture assignment for some image domain element $\mathbf{x} \in \Omega$ respectively and let $\bm{\pi} $ be a random variable with support $ [0,1]^K$ be mixture probabilities where entry $\bm{\pi}_k$ indicates the probability of belonging to the $k^{th}$ image segment. Assuming a Bayesian setting, $ Z(\mathbf{x}) | \bm{\pi}\sim \mathbf{Categorical}(\bm{\pi})$ is a latent variable such that event $Z(\mathbf{x})_k  = 1$ indicates that $\mathbf{x}$ belongs to the $k^{th}$ image segment and there is a Dirichlet prior on mixture probabilities, i.e. $ \bm{\pi} \sim \mathbf{Dirichlet}(\beta)$. The joint intensity and segment assignment probability density function at a specific $\mathbf{x} \in \Omega$ has the following form
 \begin{align}\label{E2.15} 
\mathbb{P}(I(\mathbf{x})=x  , Z(\mathbf{x}) = \mathbf{z}  | \bm{\pi}, \bm{\mu} ,  \bm{\sigma} ) &= \mathbb{P}(I(\mathbf{x})=x\; | Z(\mathbf{x})= \mathbf{z} , \bm{\mu}, \bm{\sigma})\mathbb{P}( Z(\mathbf{x})= \mathbf{z}   |\bm{\pi}) f_{\pi}(\bm{\pi})  \\
&\propto \prod \limits_{k=1}^K   \left[ \mathcal{N}(\mu_k, \sigma_k ) \right] ^{\mathbf{z}_k} \prod \limits_{k=1}^K  \bm{\pi}^{\mathbf{z}_k}_k  \frac{\Gamma(K\beta)}{\Gamma(\beta)}   \prod\limits_{k=1}^K  \bm{\pi}^{\beta -1}_k\; 
\end{align}
where $\bm{\mu}$ and $\bm{\sigma}$ are mean and variance parameters respectively, for each mixture.

\subsection{Multivariate Models}

In contrast to univariate methods, multivariate methods model the intensity distribution across an entire image domain, while accounting for the long-range dependencies among pixels. Markov Random Fields (MRFs) are a class of models that specify a distribution across the image domain by first discretizing the domain such that
\begin{align}\label{E2.9} 
\Omega = \{1,...,M\}^d 
\end{align}
for some $M \in \mathbb{N}$ and $d \in \{2,3\}  $. Following this discretization, the segmentation and intensities are indexed by vertices of an undirected graph and with adjacent vertices corresponding to adjacent pixels [\citenum{glocker2011random}]. A detailed review of MRFs is given in Chapter 4. We first assume that the mean intensity estimates for each segment class are given, let $\mu_k$ be the mean intensity estimate for the $k^{th}$ image segment. We can then define a functional which corresponds to the negative $\log$ probability of the model and has the following form
\begin{align}\label{E2.9} 
H[Z] = \sum \limits_{k = 1}^K   \sum \limits_{\mathbf{x} \in \Omega }  \mathbbm{1} \left(   Z(\mathbf{x} ) = k \right) \left[   \frac{I( \mathbf{x}   ) - \mu_k    }{\sigma_k} \right]^2   +  \lambda \sum \limits_{\mathbf{x} \in \Omega } \sum \limits_{\mathbf{y} \in N(\mathbf{x} )  } \delta_Z(\mathbf{x} ,\mathbf{y} )
\end{align}
where $N(\mathbf{x} )$ is the set of vertices adjacent to $\mathbf{x}$, $\mathbbm{1}(\cdot )$ has the following form
\begin{align}\label{E2.10} 
\mathbbm{1}(P)= 
     \begin{cases}
       1 &\quad\text{if } \text{ proposition } P \text{ is true}    \\
       0  &\quad\text{otherwise}\\
     \end{cases}
\end{align}
and $\delta_Z(\cdot, \cdot )$ has the following form
\begin{align}\label{E2.10} 
 \delta_Z(\mathbf{x} ,\mathbf{y} )= 
     \begin{cases}
       1 &\quad\text{if } Z(\mathbf{x} ) \neq Z(\mathbf{y} )     \\
       0  &\quad\text{otherwise}\;.\\
     \end{cases}
\end{align}
In practice, $\mu_k$ and $\sigma^2_k$  are typically obtained through a partial labelling (semi-supervised) done by a domain expert. In Equation \ref{E2.9} the first double summation penalizes a $Z( \cdot )$ which labels pixels whose intensities deviate greatly from the mean intensity in that segment class. For segment class, the distance from the mean is normalized by the standard deviation so that the proximity to the mean across segment classes is comparable. The second double summation favours a $Z( \cdot )$ which gives neighbouring pixels the same label and $\lambda$ is a parameter that balances both double summations. 


\subsection{Inference and Learning}

As previously mentioned, in the context of generative models, image segmentation is a statistical problem in which the segmentation is inferred and the parameters governing the generative model are learned. In this sub-section, we give examples of common inference problems in image segmentation.


\subsubsection{MLE and MAP }

In the case where we have access to a tractable likelihood or posterior distribution, one can do this inference by a maximum likelihood estimation (MLE) or maximum a posteriori (MAP) estimation of the segment assignments. More formally, assuming a Bayesian context, suppose we have access to a tractable and reasonably well-behaved posterior $p(Z|I)$ where $Z$ and $I$ are the segmentation MAP and the image, respectively. A MAP estimate of the segmentation MAP would have the following form
\begin{align}\label{E2.15} 
\hat{Z}_{MAP} = \argmax \limits_{Z} p(Z|I)\;.
\end{align}
In the case of MRFs, with the help of a result known as \textit{Hammersley-Clifford-Theorem} [\citenum{grimmett1973theorem}] which we present in Chapter 4, it can be shown that the energy functional in Equation \ref{E2.9}) uniquely determines a well-defined posterior distribution such that 
\begin{align}\label{E2.15} 
\hat{Z}_{MAP} = \argmin \limits_{Z} E[Z]\;.
\end{align}
The posterior distribution of a segmentation is not always easy to directly sample from, this is particularly true for MRFs. In these scenarios, graph-based Markov chain Monte Carlo (MCMC) methods are typically used. More specifically, Gibbs sampling is generally used for MRFs [\citenum{ko2019accelerating}] since they are a special case of a conditional random field (CRF) making it relatively easy to specify each segment assignment as a conditional probability.


\subsubsection{Variational Inference}


Rather than sampling an intractable posterior, one can use a method known as  variational inference (VI) to approximate the posterior with a distribution that comes from a family of tractable distributions. This family of tractable distributions are called variational distributions—named after \textit{variational calculus}. Once the family of distributions are specified one can approximate the posterior by finding the variational distribution that optimizes some metric between the posterior and itself. The most common metric used to measure the similarity between two distributions is the Kullback-Leibler ($KL$) divergence and it is defined as follows
\begin{align}\label{E6} 
KL(q||p) = \E \left[ \log \frac{q(z)}{p(z | x) } \right]= \int_z q(z) \log \frac{q(z)}{p(z | x) }
\end{align}\\
where $q(\cdot)$ is an approximate density and $p(\cdot)$ is the true density over the same support. Inferring the latent segmentation through this distributional approximation can be formulated as a variational Bayesian expectation maximization (VBEM) inference problem [\citenum{blaiotta2016variational}]. For a deeper analysis of VI the interested reader can consult section 4 of [\citenum{blei2017variational}].




\section{Deep Learning Methods}


In recent years deep learning (DL) methods have been successfully applied to many learning tasks and in computer vision, in particular, they have been shown to outperform previous state-of-the-art machine learning techniques [\citenum{voulodimos2018deep}]. Loosely inspired by computational models of biological learning, DL methods allow efficient and highly parallelizable computational models of multiple processing layers which implicitly learn data representations [\citenum{goodfellow2016deep}]. The structural configurations of these processing layers are known as architectures and some of the architectures that are prominent in computer vision include generative adversarial networks (GAN) [\citenum{creswell2018generative}], recurrent neural networks (RNN)  [\citenum{rumelhart1985learning}] and convolutional neural networks (CNN) [\citenum{fukushima1982neocognitron}], with CNNs performing particularly well in image segmentation tasks. The authors of [\citenum{kamnitsas2017efficient}] applied a 3D CCN to brain lesion segmentation and were able to improve on the state-of-the-art and get top performance on public benchmark datasets BRATS 2015 [\citenum{menze2014multimodal}] and ISLES 2015 [\citenum{maier2017isles}]  which are public datasets used in brain lesion segmentation challenges.



\subsection{Convolutional Neural Networks}

Presently, Convolutional Neural Networks (CNNs) are considered the state-of-the-art networks for supervised DL image segmentation problems [\citenum{minaee2021image}]. Their architecture is inspired by a hierarchical receptive field model of the visual cortex and generally includes the composition of three types of layers. Convolutional layers, where a kernel (filter) is convolved over inputs to extract a hierarchy of features, nonlinear layers which allow inputs to be mapped to feature spaces and pooling layers which reduce the spatial resolution by aggregating local information. Each layer is made of processing units that are locally connected and these local connections are called receptive fields. The layers are typically composed to form a multi-resolution pyramid in which higher-level layers learn features from wider receptive fields. The model parameters are typically learned through a stochastic version of the backpropagation algorithm [\citenum{rojas1996backpropagation}], which is a gradient-based optimization routine that efficiently propagates the gradient of the residual through the network.






\section{Other Methods}


Many segmentation methods cannot naturally be categorised into generative models or deep learning methods. In this section, we provide three examples of such classes of methods, intensity thresholding, region-based and deformable shape methods. Without loss of generality, for the remainder of the chapter, we will assume that the segmentation task is binary.

\subsection{Intensity Thresholding}

Intensity thresholding is a conceptually simple image segmentation method in which a pixel is classified into one of the segment classes if its  intensity is within a specified interval. These methods are typically used to segment between the foreground and the background of an image since in many applications of image processing, the average grayscale value of pixels belonging to an object of interest is substantially different than the average grayscale value of the pixels belonging to the background [\citenum{sezgin2004survey}]. More formally, for a given scalar threshold $\gamma$, we have the following segmentation map
\begin{align}\label{E2.3-thresh} 
Z(\mathbf{x}) = 
     \begin{cases}
       1 &\quad\text{if } I(\mathbf{x}) > \gamma    \\
       0  &\quad\text{otherwise}\;.\\
     \end{cases}
\end{align}

\subsubsection{ Otsu's Method}

To fully automate thresholding, one can implement an algorithm that automatically finds an optimal threshold $\gamma$. Many such algorithms use the histogram of the image intensities to estimate an intensity PMF in order to find an optimal $\gamma$, we will present one of them—\textit{Otsu's method} [\citenum{otsu1979threshold}]. This method is closely related to Fisher’s Discriminant Analysis (FDA) with the main difference being that Otsu’s method is unsupervised, whereas FDA is supervised. Suppose $p(\cdot)$ and $p( \cdot )$ are the intensity CDF and PMF respectively  of some image. Further, suppose $G$ is the set of intensities and that the intensity values are bounded. We can define the mean and variance of segment class 0 as a function of the threshold as follows
\begin{align}\label{E2.4} 
\mu_0 (\gamma) = \sum \limits_{  g \in G: g \leq \gamma }  g \cdot  p(g) 
\end{align}
and
\begin{align}\label{E2.5} 
\sigma^2_0 (\gamma) = \sum \limits_{  g \in G: g \leq \gamma }  (g-\mu_0 (\gamma) )^2 \cdot  p(g) 
\end{align}
the mean and variance of the second segment class are defined similarly by taking the sum over intensities that are greater than the threshold, namely
\begin{align}\label{E2.6} 
\mu_1 (\gamma) = \sum \limits_{  g \in G: g > \gamma }  g \cdot  p(g) 
\end{align}
and
\begin{align}\label{E2.7} 
\sigma^2_1 (\gamma) = \sum \limits_{  g \in G: g > \gamma }  (g-\mu_1 (\gamma) )^2 \cdot  p(g)  \;.
\end{align}
Otsu's method is an algorithm  that exhaustively searches for the threshold that minimizes the intra-class variance \textit{Otsu's method} [\citenum{otsu1979threshold}], defined as a weighted sum of variances of the two classes. The optimal $\gamma$ is retrieved by solving the following optimization problem
\begin{align}\label{E2.8} 
\gamma^* = \argmax_{\gamma}   \bigg\{ \frac{P(\gamma)[1-P(\gamma)] [\mu_0 (\gamma)  - \mu_1 (\gamma) ]^2    }{    P(\gamma) \sigma^2_0(\gamma) + [1-P(\gamma)]   \sigma^2_1(\gamma)           } \bigg\}\;.
\end{align}









\subsection{Region-Based Methods}

Region-based methods perform segmentation by attempting to find regions within an image such that within each region, some notion of similarity is maximized while simultaneously minimizing the region boundaries.

\subsubsection{Mumford-Shah Functional Model}


The Mumford-Shah functional model, first proposed by the authors of [\citenum{mumford1989optimal}], is a segmentation model that retrieves an image segmentation by computing an optimal piecewise-smooth or piecewise-constant function approximation to the original image. The method aims to find a partition of the image domain $\Omega$.
\begin{align}\label{E2.12} 
\Omega = \Omega_0 \cup \Omega_1 \cup \mathcal{B}
\end{align}
where $\Omega_0$ and $\Omega_1$ are image domain partitions belonging to each respective segment class and $ \mathcal{B}$ is the set of edge pixels, pixels belonging to the shared boundaries between the image segments. The objective is to find a piece-wise function that is constant within the  $\Omega_k$ but varies discontinuously and greatly across most of the boundary $\mathcal{B}$ between different $\Omega_k$. Rather than mapping pixels to the label set $\{ 0, 1  \}$, we will consider the following piecewise-constant segmentation map
\begin{align}\label{E2.13} 
 Z(\mathbf{x})= 
     \begin{cases}
       a &\quad\text{if } \mathbf{x} \in \Omega_0    \\
       b &\quad\text{if } \mathbf{x} \in \Omega_1   \\
     \end{cases}
\end{align}
where $a,b \in \mathbb{R}$ and we will consider the following functional
\begin{align}\label{E2.14} 
E[Z, \mathcal{B}]  = \int \limits_{\Omega \backslash  \mathcal{B}} \left( I(\mathbf{x}) -  Z(\mathbf{x})    \right)^2  d \mathbf{x} + \lambda | \mathcal{B} |\;.
\end{align}
The integral in Equation 2.3.9 attempts to quantify how well the piece-wise constant segmentation approximates the original image. The second term, the cardinality of the edge-pixel set, quantifies the number of allowable edges within the segmentation and $\lambda$ is again a balancing parameter. To retrieve the optimal segmentation, one has to minimize the functional in Equation 2.3.9. In principle, the segmentation that minimizes this functional, will approximate the original image well, all the while minimizing the boundaries between the segment, which effectively minimized the number of disconnected segments and maximizes the area (or volume) of the connected segments [\citenum{pock2009algorithm}].


\subsection{Deformable Shape Methods}

Deformable shape methods perform segmentation by relying on the registration of an image and an annotated template image, sometimes called an \textit{atlas}. Once both images are aligned, regions that are pre-segmented on the template allow automatic segmentation of the source image. Suppose $\phi$ is a deformation that geometrically aligns image contents of a source image $I$ and an atlas $T$ such that
\begin{align}\label{E2.14} 
I = T \circ \phi^{-1}\;.
\end{align}
In Chapter 3, we give a more rigorous formulation of such a deformation. Further, suppose that $T$ has a corresponding segmentation $Z_T$. Under this formulation, the segmentation for the image $I$ would be 
\begin{align}\label{E2.14} 
Z_I = Z_T \circ \phi^{-1}\;.
\end{align}


\section{Evaluation Methods}

Generally, supervised segmentation evaluation methods attempt to quantify the degree of overlap between an estimated and ground truth segmentation. Using the map notation of a segmentation in 2.0.1, we can equivalently understand the segmentation as a set with the image of its map, i.e. $Z(\Omega)$. \\

The Dice score is one of the most popular and conceptually easy to understand segmentation evaluation methods. For two segmentations $A$ and $B$, the Dice score is calculated as follows
\begin{align}\label{E-DICE} 
D(A,B) = \frac{2| A \cap B|}{ |A| + |B|}\;.
\end{align}  
Where the numerator accounts for the segmentations' overlap and the denominator accounts for the sizes of the segmentations. The Jaccard coefficient is another segmentation evaluation method and is calculated as follows
\begin{align}\label{E-DICE} 
J(A,B) = \frac{| A \cap B|}{ | A \cup B|}\;.
\end{align}  
$J$ and $D$ are in some sense equivalent and are related through the following expression
\begin{align}\label{E-DICE} 
D = \frac{2J}{1+J}\;.
\end{align}  
$D$ is known to yield higher values for larger volumes. Another segmentation evaluation method is the average Hausdorff distance, which is especially recommended for segmentation tasks with complex boundaries and small thin segments and compared to the Dice score, the average Hausdorff distance has the advantage of accounting for localisation when considering segmentation performance [\citenum{aydin2021usage}]. For two segmentations $A$ and $B$, which are non-empty subsets of a metric space $\big(S, d \big)$, the Average Hausdorff distance is calculated as follows
\begin{align}\label{E-DICE} 
H (A,B) = \frac{1}{2}\Big(  \frac{1}{|A|}\sum \limits_{x \in A}    \min \limits_{y \in B} d(x,y) +   \frac{1}{|B|}\sum \limits_{y \in B}    \min \limits_{x \in A} d(x,y)   \Big)\;.
\end{align}  
For more evaluation methods, the interested reader can consult [\citenum{taha2015metrics}].

