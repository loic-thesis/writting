

In this final chapter, we demonstrate experiments involving the application of RegSeg to a medical neuroimaging ischemic stroke lesion dataset. We will use terminology consistent with Chapter 5 Section 3 and refer to pairs of images as \textit{subject} and \textit{template} and refer to the ground truth segmentation as a true mask. To evaluate the registration performance, we will follow the same protocol as in Chapter 5 Section 3, we will consider 3 scenarios, registration with no mask, registration with the ground truth mask and registration with RegSeg and all evaluations will take place in subject space using a sum of squared differences (SSD) masked with the ground truth segmentation. In the MR application, this means that  we will warp the healthy template  image onto the subject image and compare how well the healthy tissue regions align. The SSD will again be computed over the entire healthy tissue and a region of healthy tissue near the lesion boundary. Evaluating the segmentation performance will also follow the same protocol as in Chapter 5 Section 3, in which we compare segmentation estimate results acquired through different algorithms with the ground truth segmentation through their Dice score.





\section{Dataset} 

The final experiment considers warping a stroke lesion subject image onto a healthy symmetric template image. The subject images we used were collected through a clinical routine acquisition of a 28 subject sub-acute ischemic stroke lesion segmentation (SISS) dataset from the 2015 Ischemic Stroke Lesion Segmentation (ISLES) [\citenum{maier2017isles}], a medical image segmentation challenge at the International Conference on Medical Image Computing and Computer Assisted Intervention (MICCAI). This segmentation challenge aims to provide a platform for a fair and direct comparison of methods for ischemic stroke lesion segmentation from multi-spectral MRI images. Each sample was segmented by 1 expert and included many MRI sequences, FLAIR, T2w TSE, T1w TFE/TSE and DWI; our algorithm was evaluated on the T1w. All MRI modalities were skull-stripped, re-sampled to an isotropic spacing of $1^3 mm$ and co-registered to the FLAIR modality. Axial slices of a sample of 6 subjects along with their corresponding lesion masks are displayed in Figure 6.1.\\


\begin{figure}[htp]
  \centering
    \includegraphics[width=\textwidth]{../figures/extra_figs/dataset2}} 
  \caption{Axial slices from 5 subjects, with corresponding lesion masks, from the 2015 Ischemic Stroke Lesion Segmentation (ISLES) dataset [\citenum{maier2017isles}]. The two columns to the right display brain-lesion overlays with two different translucencies. }
  \label{fig:square-I-T}
\end{figure}






 \begin{table}[htp]

\begin{center}

        \begin{tabular}{|p{7.5cm}|p{7.5cm}|  }   \hline
          

               \textbf{Lesion volume} &   $\mu = 17.59ml \;\; [0.001,346.064]$      \\ \hline
               \textbf{Haemorrhage present} &   $n=12\;\;0=yes, 1=no$      \\ \hline
               \textbf{Lesion localization (lobes)} &   $n_1=11,n_2=24,n_3=42,n_4=17,n_5=2,n_6=6 \;\;1=frontal, 2=temporal, 3=parietal, 4=occipital, 5=midbrain, 6=cerebellum$      \\ \hline
               \textbf{Lesion localization (cortical / subcortical)} &   $n_1=36,n_2=49\;\;1=cortical, 2=subcortical$      \\ \hline                               
                \textbf{Affected artery} &   $n_1=6,n_2=45,n_3=11,n_4=5,n_5=0\;\;1=ACA, 2=ACM, 3=ACP, 4=BA, 5=other$      \\ \hline
               \textbf{Midline shift observable} &   $n_0=51,n1=5,n2=0 \;\;0=none, 1=slight, 2=strong$      \\ \hline
               \textbf{Ventricular enhancement observable} &   $n_0=38,n1=15,n2=3 \;\;0=none, 1=slight, 2=strong$      \\ \hline
               
                               
            \end{tabular}
        \caption{\label{tab:table-reg-result} For SISS, cases with sub-acute ischemic stroke lesions have been collected from clinical routine acquisition. The following table of characteristics reflect the diversity of the SISS dataset. Table taken from [\citenum{maier2017isles}, Tbl. 3].}

\end{center}
    \end{table}

The template is a symmetric template acquired from UCLA's International Consortium for Brain Mapping (ICBM) centre. The template is an unbiased non-linear average of images from the MNI152 database and has been generated in a way that combines the attractions of both high spatial resolution and signal-to-noise while not being subject to the specificities of any single brain [\citenum{fonov2011unbiased}]. The template was also re-sampled to an isotropic spacing of $1^3 mm$ and included T1w, T2w and PDw modalities; we again used the T1w modality. A pair of subject and template axial image slices is displayed in Figure 6.3.\\


\begin{figure}[H]
  \centering
     \subcaptionbox{ (a) Subject }{\label{fig:square-warped-a}\includegraphics[width=.28\textwidth ]{../figures/experiments/MR/source}} 
     \subcaptionbox{ (b) Template }{\label{fig:square-warped-a}\includegraphics[width=.28\textwidth ]{../figures/experiments/MR/target}} 

    \caption{ Subfigure (a) is a subject image from a sub-acute ischemic stroke lesion (SISS) dataset and subfigure (b) is a symmetric template acquired from UCLA's International Consortium for Brain Mapping (ICBM) centre. }
  \label{fig:square-I-T}
\end{figure}

We also used a stroke lesion probabilistic spatial mapping that was constructed by averaging over all samples from a dataset of 304 MRI images from 11 cohorts worldwide which were collected from research groups in the ENIGMA Stroke Recovery Working Group consortium [\citenum{liew2022enigma}]; the images consisted of T1w MRIs of individuals post stroke. These images were collected primarily for research purposes and are not representative of the overall general stroke population. The spatial prior was normalized to the MNI-152 template and therefore the spatial prior naturally overlap the template [\citenum{liew2018large}].\\

\begin{figure}[H]
  \centering
          \subcaptionbox{ (a) Axial view  }{\label{fig:square-warped-a}\includegraphics[width=.23\textwidth ]{../figures/prior_map/axial}} 
          \subcaptionbox{ (b) Coronal view }{\label{fig:square-warped-a}\includegraphics[width=.28\textwidth ]{../figures/prior_map/coronal}} 
          \subcaptionbox{ (b) Sagittal view }{\label{fig:square-warped-a}\includegraphics[width=.28\textwidth ]{../figures/prior_map/sagittal}} 

    \caption{ A stroke lesion probabilistic spatial mapping that was constructed by averaging over all samples from a dataset of 304 MRI images from 11 cohorts worldwide which were collected from research groups in the ENIGMA Stroke Recovery Working Group consortium [\citenum{liew2022enigma}]. Brighter regions are more probable regions. }
  \label{fig:square-I-T}
\end{figure}


\section{Preprocessing} 

The template and subject images have very different intensity distributions, as can be seen in Figure 6.3 and 6.4. This is to be expected since MR intensity has no intrinsic anatomical interpretation and instead varies between different MR scanners and scanner protocols. We normalize the intensities in a preprocessing step, by applying a piecewise affine histogram matching algorithm proposed by [\citenum{reinhold2019evaluating}]. To do this, we first calculate a standard histogram that is fitted to the template through intensity histogram percentiles at $1,10,20,...,90,99$ percent, where the intensity values below 1\% and above 99\% are discarded as outliers and the 1\% tail-ends are linearly interpolated. We then normalize the subject image by matching its intensity histogram to the standard histogram. To do this, we first calculate the same set of percentiles and use them to segment the image histogram into deciles. The deciles are then piecewise linearly mapped to the corresponding intensities in the standard histogram's scale landmarks. Pre-normalization and post-normalization subject and template intensity histograms are displayed in Figure 6.4.\\

\begin{figure}[H]
  \centering
    \includegraphics[width=\textwidth]{../figures/extra_figs/normalized_intensities}} 
  \caption{ Subject intensity value normalization through a piecewise affine histogram matching algorithm [\citenum{reinhold2019evaluating}].}
  \label{fig:square-I-T}
\end{figure}



Once the subject image intensities are normalized, we used the dataset as an unsupervised training set to learn $\Theta$, the parameter that characterizes the lesion intensity distribution introduced in Chapter 5, Section 1, Equation 5.1.20. Originally, we intended to learn this parameter during the \textit{RegSeg} runtime but due to implementation difficulties and in the interest of time we decided to learn it directly from the dataset using the ground-truth lesion masks. We acknowledge that learning this parameter from the validation set is advised against, however, this is the only parameter learned from this dataset, and we do not believe that we would have gotten very different results if the lesion intensity distribution were determined in another way, and for later publication, we will carefully consider a Training/Validation/Test split. To estimate this parameter, we modelled the lesion distinctions as a mixture of two Gaussians. Figure 6.5 shows the frequency histogram of the lesion intensity values.\\








\begin{figure}[H]
  \centering
    \includegraphics[width=\textwidth]{../figures/extra_figs/lesion_intensity_dist}} 
  \caption{ The frequency histogram of the lesion intensity values once the dataset was normalized.}
  \label{fig:square-I-T}
\end{figure}

\begin{figure}[h]
  \centering
     \subcaptionbox{ (a) Lesion boundary }{\label{fig:square-warped-a}\includegraphics[width=.48\textwidth ]{../figures/experiments/MR/hypo_hyper_1}} 
     \subcaptionbox{ (b) Lesion boundary }{\label{fig:square-warped-a}\includegraphics[width=.48\textwidth ]{../figures/experiments/MR/hypo_hyper_2}} 

  \caption{ The lesion has a hyper-intense inner region that is outlined in green and a hypo-intense outer region that is between the red and green outline. Sub-figure (a) displays the entire lesion boundary.}
  \label{fig:square-I-T}
\end{figure}

Finally, the subject images were re-sampled to match the size of the template and then affinely registered to the template. Once the subject images were co-registered to the template, a 2D axial slice is taken from all subjects and template to in order to get a 2D dataset to perform our proof-of-concept experiments, the size of each image was $197×220$ pixels. We excluded 3 subjects from the dataset whose lesion was in the cerebellum since this location made it hard to retrieve an axial slice that was comparable to the other subjects, leaving 25 subject images.


\section{Results}


Images (a) and (b) in Figure 6.9 demonstrates the shrinkage distortion due to the absence of a lesion mask, just as observed in the geometric shapes experiment presented in Chapter 5, Section 3. The mask effectively behaves as an outlier remover. Images (c) and (d) in Figure 6.9 demonstrates the inverse behaviour, when applying the inverse warp to bring the template into subject coordinates, the shrinkage corresponds to a hyper expansion of the same region.\\

As seen in Figure 6.13 (c) and (f), the mask estimate retrieved through \textit{RegSeg-Morph} was found to be generally smaller than the true mask and is concentrated in the middle of it. This is due to some lesions having hyper-intense inner regions and hypo-intense outer regions, as displayed in Figure 6.7. This poses a challenge to the segmentation model since the outer region closely resembles white matter and modelling this particular subtlety through intensity alone would require learning a highly precise multi-modal lesion distribution. Moreover, some geometric information could help estimate a larger and more accurate lesion boundary. In addition to the mismatch intensities of lesion tissue, the presence of a lesion geometrically distorts nearby structures. For instance in Figure 6.13 (d) and (e), the presence of the lesion in the left cerebral hemisphere pushed against the left lateral vesicle, making the left frontal horn smaller than the right one.\\




Figures 6.11 (a) - (c ) display how the mask estimates evolve over iterations and converges to a region around the lesion. Just as in the second experiment, we can observe that early in the registration, the misaligned grey and white matter corrupts the mask estimate and this is corrected for as the registration goes forward. It’s worth noting that the mislabelling due to overlapping grey and white matter creates many disconnected regions that are significantly smaller than the true lesion area. A segmentation regularization that penalizes adjacent pixels that do not share labels would correct for this lack of size and connectivity and such a regularization could be naturally modelled through an MRF, effectively removing false positives.\\







\begin{figure}[H]
  \centering
     \subcaptionbox{ (a) $I \circ \phi $ (not masked) }{\label{fig:square-warped-a}\includegraphics[width=.24\textwidth ]{../figures/experiments/MR/warped_source_no_mask}} 
     \subcaptionbox{ (b) $I \circ \phi $ (masked) }{\label{fig:square-warped-a}\includegraphics[width=.24\textwidth ]{../figures/experiments/MR/warped_source_mask}} 
         \subcaptionbox{ (c) $T \circ \phi^{-1} $ (not masked) }{\label{fig:square-warped-a}\includegraphics[width=.24\textwidth ]{../figures/experiments/MR/target_bullback_no_mask}} 
     \subcaptionbox{ (d) $T \circ \phi^{-1} $ (masked) }{\label{fig:square-warped-a}\includegraphics[width=.24\textwidth ]{../figures/experiments/MR/target_bullback_mask}} 
     
     
     
      \subcaptionbox{ (e) $ \phi $ (not masked)  }{\label{fig:square-warped-a}\includegraphics[width=.24\textwidth ]{../figures/experiments/MR/warp_no_mask}} 
      \subcaptionbox{ (f) $ \phi $ (masked)  }{\label{fig:square-warped-a}\includegraphics[width=.24\textwidth ]{../figures/experiments/MR/warp_mask}} 
            \subcaptionbox{ (g) $ \phi^{-1} $ (not masked)  }{\label{fig:square-warped-a}\includegraphics[width=.24\textwidth ]{../figures/experiments/MR/inv_warp_no_mask}} 
      \subcaptionbox{ (h) $ \phi^{-1}$ (masked)  }{\label{fig:square-warped-a}\includegraphics[width=.24\textwidth ]{../figures/experiments/MR/inv_warp_mask}} 

  \caption{The only difference between the warped images in (a) and (b) and the warped images in (c) and (d) is the presents of a mask. Every other parameter was held fixed. }
  \label{fig:square-I-T}
\end{figure}

\begin{figure}[H]
  \centering
     \subcaptionbox{ (a) RegSeg-M ($t=1$)}{\label{fig:square-warped-a}\includegraphics[width=.24\textwidth ]{../figures/extra_figs/mask_regseg_morph_1}} 
     \subcaptionbox{ (b) RegSeg-M ($t=10$)}{\label{fig:square-warped-a}\includegraphics[width=.24\textwidth ]{../figures/extra_figs/mask_regseg_morph_15}}
     \subcaptionbox{ (c) RegSeg-M ($t=20$)}{\label{fig:square-warped-a}\includegraphics[width=.24\textwidth ]{../figures/extra_figs/mask_regseg_morph_30} }
     \subcaptionbox{ (d) True mask }{\label{fig:square-warped-a}\includegraphics[width=.24\textwidth ]{../figures/extra_figs/true_mask}}
     
     \subcaptionbox{ (e) RegSeg ($t=1$)}{\label{fig:square-warped-a}\includegraphics[width=.24\textwidth ]{../figures/extra_figs/mask_regseg_1}}
     \subcaptionbox{ (f) RegSeg ($t=10$)}{\label{fig:square-warped-a}\includegraphics[width=.24\textwidth ]{../figures/extra_figs/mask_regseg_15}}
     \subcaptionbox{ (g) RegSeg ($t=20$)}{\label{fig:square-warped-a}\includegraphics[width=.24\textwidth ]{../figures/extra_figs/mask_regseg_30}} 
     \subcaptionbox{ (h) Lesion prior }{\label{fig:square-warped-a}\includegraphics[width=.24\textwidth ]{../figures/extra_figs/lesion_spatial_prior}} 

  \caption{ Subfigures (a), (b) and (c) display mask estimates using the RegSeg-Morph algorithm and at different iterations. Subfigures (e), (f) and (g) display mask estimates using the RegSeg algorithm and at different iterations. The images use subject ID = 1.  }
  \label{fig:square-I-T}
\end{figure}



\begin{figure}[H]
  \centering
     \subcaptionbox{ (a) Mask estimate ($t=1$)}{\label{fig:square-warped-a}\includegraphics[width=.32\textwidth ]{../figures/experiments/MR/regseg_mask_1}} 
     \subcaptionbox{ (b) Mask estimate ($t=10$)}{\label{fig:square-warped-a}\includegraphics[width=.32\textwidth ]{../figures/experiments/MR/regseg_mask_2}} 
     \subcaptionbox{ (c) Mask estimate ($t=20$)}{\label{fig:square-warped-a}\includegraphics[width=.32\textwidth ]{../figures/experiments/MR/regseg_mask_3}} 
     
     \subcaptionbox{ (d) Lesion/brain overlay}{\label{fig:square-warped-a}\includegraphics[width=.32\textwidth ]{../figures/experiments/MR/overlay_1}} 
     \subcaptionbox{ (e) Lesion/brain overlay}{\label{fig:square-warped-a}\includegraphics[width=.32\textwidth ]{../figures/experiments/MR/overlay_2}}      
     \subcaptionbox{ (f) True mask}{\label{fig:square-warped-a}\includegraphics[width=.32\textwidth ]{../figures/experiments/MR/true_mask}} 

  \caption{ Subfigures (a), (b) and (c) display mask estimates using RegSeg-Morph and at different iterations. The subfigures (d) and (e) display the lesion mask overlaid on the corresponding brain with different translucencies. The images use subject ID = 2.  }
  \label{fig:square-I-T}
\end{figure}




For the remainder of this section we present experimental results. We observed a statistically significant improvement in performance, for both registration and segmentation, when our algorithm is used as opposed to a sequential application of registration and segmentation. In segmentation experiments in particular, \textit{RegSeg-Morph} improved the average Dice score by 208.8\%; the performance between \textit{RegSeg-Morph}  and \textit{RegSeg} was not statistically significant. For the registration results, the performance around the region near the lesion boundary was particularly improved. 


\begin{center}
    \begin{table}[H]
    \centering
        \begin{tabular}{|p{3.2cm}| p{1.5cm}|p{1.8cm}| p{2.3cm}| p{2.3cm}| p{2.4cm}|  } 
            \hline
                \textit{Algorithm} & \textbf{Mean}&   \textbf{Median }&   \textbf{Minimum }&  \textbf{Maximum }   & \textbf{Average }   \\ [0.5ex] 
                                             & \textit{Dice}&   \textit{Dice}&   \textit{Dice }&   \textit{Dice } &      \textbf{run time }   \\ [0.5ex] 
                \hline
                
                
                \textbf{Seg}     &            0.1603      &   0.162       &          0.0147   &       0.331 &       28 seconds     \\ \hline                                
                \textbf{RegSeg} &            0.276      &  0.264          &      0.062    &        0.436 &        8.5 minutes          \\ \hline                            
                \textbf{RegSeg-Morph} &     \textbf{0.298}   &   \textbf{0.291}      &           \textbf{0.1} &        \textbf{0.467}  &        8.9 minutes     \\ \hline
                      
            \end{tabular}
        \caption{\label{tab:table-seg-result} Segmentation results (higher is better). Given are different Dice scores for the \textit{Seg}, \textit{RegSeg} and \textit{RegSeg-Morph} algorithms. The Average run times were calculated for 1 iteration of  \textit{Seg} and 25 iterations of  \textit{RegSeg}  and  \textit{RegSeg-Morph}.}
    \end{table}
\end{center}


\begin{center}
    \begin{table}[H]
    \centering
        \begin{tabular}{|p{3.3cm}| p{2.7cm}|p{1.4cm}| p{1.7cm}| p{2.3cm}|p{2.3cm}|  } 
            \hline
                \textit{Algorithm} & \textbf{Region}& \textbf{Mean}&   \textbf{Median}&   \textbf{Minimum}&   \textbf{Maximum } \\ [0.5ex] 
                                             &                            & \textit{SSD}        &   \textit{SSD}&    \textit{SSD}     &    \textit{SSD} \\ [0.5ex] 
                \hline
                
                
                \textbf{Reg} & \textit{Whole image} &       516.4     &     514.34         &      388.4  &      688.9        \\
                                & \textit{Near lesion} &                        222.9     &     213.5        &      137.9 &      311.9    \\ \hline
                                
                \textbf{Reg-True-Mask} & \textit{Whole image} &     381.6   &       363.7         &      243.3    &       512.9            \\
                                & \textit{Near lesion} &                         111.5   &      119.09         &        23.55   &       186.01 \\ \hline
                
                
                \textbf{RegSeg} & \textit{Whole image} &         473.8       &    499.6          &        319.8  &       636.8      \\
                                & \textit{Near lesion} &                        129.13      &      130.66         &          24.2 &          216.3   \\ \hline
                
            \end{tabular}
        \caption{\label{tab:table-reg-result}Registration results (lower is better). Given is the sum of squared differences (SSD) between the source image in subject coordinates, i.e. $I$, and the template image in subject coordinates, i.e. $T \circ \phi^{-1}$  for the \textit{Reg}, \textit{Reg-True-Mask} and \textit{RegSeg} algorithms. SSD is computed outside the segmentation mask this corresponds to the healthy tissue in the case of lesion segmentation. Moreover, it is computed for two distinct regions, one near the segmentation boundary and one over the entire image. Each algorithm ran for 25 iterations with \textit{Reg} taking an average runtime of 3 minutes and \textit{RegSeg}  and  \textit{RegSeg-Morph} taking an average runtime of 4 minutes.}
    \end{table}
\end{center}




\begin{center}
    \begin{table}[H]
    \centering
        \begin{tabular}{|p{5.9cm}|p{1.7cm}|  } 
            \hline
                \textit{Algorithms (Segmentation)} &  \textit{p-value }  \\ [0.5ex] 
                \hline
                
                
                \textbf{RegSeg vs. Seg }     &        0.015         \\ \hline                                
                \textbf{RegSeg-Morph vs. Seg }   &       0.004          \\ \hline                            
                \textbf{RegSeg-Morph vs. RegSeg }  &       0.616       \\ \hline
                      
            \end{tabular}
        \caption{\label{tab:table-seg-result} We performed a two-sided $t$-test to compare the segmentation performance of different algorithms. These particular tests were done with the Dice scores of 25 subjects. We excluded 3 subjects from the dataset whose lesion was in the cerebellum since this location made it hard to retrieve an axial slice that was comparable to the other subjects.}
    \end{table}
\end{center}

\begin{center}
    \begin{table}[H]
    \centering
        \begin{tabular}{|p{5.9cm}| p{2.7cm}|p{1.7cm}|   } 
            \hline
              \textit{Algorithms (Registration)} & \textit{Region}&  \textit{p-value }  \\ [0.5ex] 
                \hline
                
                
                 \textbf{RegSeg vs. Reg }  & \textit{Whole image}   &     0.180             \\
                                & \textit{Near lesion} &                                       0.0009         \\ \hline
                                
                \textbf{Reg-True-Mask vs. RegSeg }  & \textit{Whole image} &         0.005                  \\
                                & \textit{Near lesion} &                                                         0.348           \\ \hline
                

            \end{tabular}
        \caption{\label{tab:table-reg-result}We performed a two-sided $t$-test to compare the registration performance of different algorithms. These particular tests were done with the SSD between the template and 25 subjects. We excluded 3 subjects from the dataset whose lesion was in the cerebellum since this location made it hard to retrieve an axial slice that was comparable to the other subjects. }
    \end{table}
\end{center}




\begin{figure}[H]
  \centering
    \includegraphics[width=0.5\textwidth]{../figures/extra_figs/similarity}} 
  \caption{ Registration and segmentation performance of the \textit{RegSeg} algorithm, showing steady improvement in both the registration metric (SSD) and the segmentation metric (DICE) over 25 iterations. All metrics are mean values over the 25 subjects.}
  \label{fig:square-I-T}
\end{figure}


