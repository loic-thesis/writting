\contentsline {section}{Table of Contents}{v}{chapter*.3}
\contentsline {chapter}{List of Figures}{ix}{chapter*.5}
\contentsline {chapter}{List of Tables}{xi}{chapter*.6}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Image Segmentation}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Generative Model-Based Methods}{7}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Univariate Models}{7}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Multivariate Models}{7}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}Inference and Learning}{8}{subsection.2.1.3}
\contentsline {subsubsection}{MLE and MAP }{8}{subsection.2.1.3}
\contentsline {subsubsection}{Variational Inference}{9}{equation.2.1.8}
\contentsline {section}{\numberline {2.2}Deep Learning Methods}{9}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Convolutional Neural Networks}{10}{subsection.2.2.1}
\contentsline {section}{\numberline {2.3}Other Methods}{10}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Intensity Thresholding}{10}{subsection.2.3.1}
\contentsline {subsubsection}{ Otsu's Method}{11}{equation.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Region-Based Methods}{12}{subsection.2.3.2}
\contentsline {subsubsection}{Mumford-Shah Functional Model}{12}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Deformable Shape Methods}{12}{subsection.2.3.3}
\contentsline {section}{\numberline {2.4}Evaluation Methods}{13}{section.2.4}
\contentsline {chapter}{\numberline {3}Image Registration}{15}{chapter.3}
\contentsline {section}{\numberline {3.1}Spatial Transformation Between Image Domains}{16}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Affine Registration}{17}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Non-Linear Registration}{17}{subsection.3.1.2}
\contentsline {subsubsection}{Low-Frequency Basis Functions}{18}{subsection.3.1.2}
\contentsline {subsubsection}{Radial Basis Functions}{18}{equation.3.1.4}
\contentsline {subsubsection}{Large Deformation Diffeomorphic Metric Mapping}{19}{equation.3.1.5}
\contentsline {section}{\numberline {3.2}Quantifying Image Alignment}{21}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Point-wise Similarity Measures}{21}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Global Similarity Measures}{21}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Regularized Similarity}{23}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}Generative Model Perspective}{24}{subsection.3.2.4}
\contentsline {chapter}{\numberline {4}Markov Random Fields}{26}{chapter.4}
\contentsline {section}{\numberline {4.1}Random Fields}{27}{section.4.1}
\contentsline {section}{\numberline {4.2}Neighbourhood Structures}{28}{section.4.2}
\contentsline {section}{\numberline {4.3}Markov Random Fields}{29}{section.4.3}
\contentsline {section}{\numberline {4.4}Inference with Markov Random Fields}{30}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}MAP estimates}{31}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}MRF Likelihood Distribution}{31}{subsection.4.4.2}
\contentsline {subsection}{\numberline {4.4.3}MRF Prior Distribution}{32}{subsection.4.4.3}
\contentsline {subsection}{\numberline {4.4.4}MRF Energy Function}{32}{subsection.4.4.4}
\contentsline {subsection}{\numberline {4.4.5}Linearization of the Energy}{33}{subsection.4.4.5}
\contentsline {section}{\numberline {4.5}Modelling with MRFs}{34}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}MRFs in Image Segmentation}{34}{subsection.4.5.1}
\contentsline {subsection}{\numberline {4.5.2}MRFs in Image Registration}{36}{subsection.4.5.2}
\contentsline {section}{\numberline {4.6}Initial Experiments}{37}{section.4.6}
\contentsline {chapter}{\numberline {5}Joint Registration and Segmentation}{38}{chapter.5}
\contentsline {section}{\numberline {5.1}Forward Model}{40}{section.5.1}
\contentsline {section}{\numberline {5.2}Inference and Parameter Optimization}{43}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Simplified Model}{43}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Maximum A Posteriori Estimation}{45}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Implementation}{45}{subsection.5.2.3}
\contentsline {subsubsection}{Deformation inference}{46}{subsection.5.2.3}
\contentsline {subsubsection}{Segmentation Inference}{46}{equation.5.2.10}
\contentsline {subsubsection}{Inference Algorithms}{47}{equation.5.2.11}
\contentsline {section}{\numberline {5.3}Synthetic Experiments}{49}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Experiment 1: Shapes}{50}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}Experiment 2: Cartoon Brain}{53}{subsection.5.3.2}
\contentsline {subsubsection}{Experimental Results}{54}{figure.5.15}
\contentsline {chapter}{\numberline {6}Experiments on Neuroimages}{55}{chapter.6}
\contentsline {section}{\numberline {6.1}Dataset}{56}{section.6.1}
\contentsline {section}{\numberline {6.2}Preprocessing}{59}{section.6.2}
\contentsline {section}{\numberline {6.3}Results}{61}{section.6.3}
\contentsline {chapter}{\numberline {7}Conclusion}{67}{chapter.7}
\contentsline {chapter}{Index}{74}{appendix*.8}
