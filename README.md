# Writing

A LaTeX codebase that renders a PDF copy of my thesis.

## Up-to-date Copy
Click here to see an up-to-date PDF copy --> [PDF copy](https://gitlab.com/loic-thesis/writting/-/blob/main/src/main.pdf)\
If hyperlink doesn't work, go to: **src/main.pdf**

## Status
* Chapter 1: Introduction  **(editing)**
* Chapter 2: Image Segmentation  **(editing)**
* Chapter 3: Image Registration **(editing)**
* Chapter 4: Joint registration and Segmentation **(editing)**
* Chapter 5: Methods, MRF section **(editing)**

## Authors 
*Loïc Muhirwa*

## Supervised by
*Tanya Schmah*

